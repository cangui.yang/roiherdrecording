﻿Imports System.Data.SqlClient

Module SQLModule


    Public Sub MultiflexrunNonQuery(ByVal strSQL As String)
        Using myConnection As New SqlConnection(My.Settings.Multiflex_Linden)
            myConnection.Open()
            Using myCommand As New SqlCommand(strSQL, myConnection)
                myCommand.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Public Sub runNonQuery(ByVal strSQL As String)
        Using myConnection As New SqlConnection(My.Settings.LFKillConnection)
            myConnection.Open()
            Using myCommand As New SqlCommand(strSQL, myConnection)
                myCommand.ExecuteNonQuery()
            End Using
        End Using
    End Sub

    Public Function getScalarStr(ByVal strSQL As String) As String
        Dim temstr As String = ""

        Using myConnection As New SqlConnection(My.Settings.LFKillConnection)
            myConnection.Open()
            Using myCommand As New SqlCommand(strSQL, myConnection)
                temstr = myCommand.ExecuteScalar
            End Using
        End Using
        Return temstr
    End Function
End Module
