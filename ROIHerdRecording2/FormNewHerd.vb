﻿Imports System.Data.SqlClient

Public Class FormNewHerd

    Public isHerdExist As Boolean
    Public ROIHerdID As Integer

    Private Sub bt_Update_Click(sender As Object, e As EventArgs) Handles bt_Update.Click
        Dim strSQL As String
        If isHerdExist Then
            strSQL = "UPDATE ROIMoveHerd SET Name = '" & tb_Name.Text.Replace("'", "''") & "', townland= '" & tb_Town.Text.Replace("'", "''") & "', county = '" &
                            tb_County.Text.Replace("'", "''") & "', LastUpdatedDateTime=GetDate() WHERE ROIHerdID = " & ROIHerdID
        Else
            strSQL = "INSERT INTO ROIMoveHerd (Herdno,Name,townland,County) VALUES('" & tb_HerdNo.Text.Replace("'", "''") & "', '" & tb_Name.Text.Replace("'", "''") & "','" &
                           tb_Town.Text.Replace("'", "''") & "', '" & tb_County.Text.Replace(",", "''") & "')"
        End If
        runNonQuery(strSQL)
        If isHerdExist Then
            MsgBox("Herd is Updated!")
        Else
            MsgBox("Herd is Created!")
        End If
        If tb_HerdNo.Text = Form1.tb_HerdNo.Text Then
            Form1.bt_AddMove.Enabled = True
        Else
            Form1.bt_AddMove.Enabled = False
        End If
        Form1.Show()
        Me.Close()
    End Sub

    Private Sub FormNewHerd_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Using myconnection As New SqlConnection(My.Settings.LFKillConnection)
                myconnection.Open()
                Using mycommand As New SqlCommand("SELECT ROIHerdID,isnull(Name,'') as Name, isnull(TownLand,'') as Town, isnull(County,'') as County FROM ROIMoveHerd WHERE herdno = '" & tb_HerdNo.Text & "'", myconnection)
                    Using reader As SqlDataReader = mycommand.ExecuteReader
                        If reader.HasRows Then
                            isHerdExist = True
                            While reader.Read
                                ROIHerdID = CInt(reader("ROIHerdID"))
                                tb_Name.Text = reader("Name")
                                tb_Town.Text = reader("Town")
                                tb_County.Text = reader("County")
                            End While
                        Else
                            isHerdExist = False
                        End If
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MsgBox("Error on Loading Herd:" & ex.Message)
        End Try
    End Sub

    Private Sub bt_Close_Click(sender As Object, e As EventArgs) Handles bt_Close.Click
        Me.Close()
        Form1.Show()

    End Sub
End Class