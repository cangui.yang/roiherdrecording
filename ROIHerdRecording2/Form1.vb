﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Runtime.InteropServices
Imports Microsoft.Office.Interop




Public Class Form1
    Dim bb As New BordBia
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'AMPSLindenBeefDataSet.AnimalQAStatus' table. You can move, or remove it, as needed.

        'TODO: This line of code loads data into the 'AMPSLindenBeefDataSet.ROIMove' table. You can move, or remove it, as needed.
        '  Me.ROIMoveTableAdapter.Fill(Me.AMPSLindenBeefDataSet.ROIMove)
        'TODO: This line of code loads data into the 'AMPSLindenBeefDataSet1.ROIMove' table. You can move, or remove it, as needed.

        'TODO: This line of code loads data into the 'AMPSLindenBeefDataSet.ROIMove' table. You can move, or remove it, as needed.
        ' Me.ROIMoveTableAdapter.Fill(Me.AMPSLindenBeefDataSet.ROIMove)
        If My.Computer.Network.Ping("10.0.3.65") = False Then
            MsgBox("Fail to Connect to Server LFKill, Please check your network connection!")
            Me.Close()
        End If
        disablebuttons()
        lb_QAStatus.Text = ""
        Me.AnimalQAStatusTableAdapter.Fill(Me.AMPSLindenBeefDataSet.AnimalQAStatus)




    End Sub

    Private Sub bt_CheckSQStatus_Click(sender As Object, e As EventArgs) Handles bt_CheckSQStatus.Click
        Try
            lb_QAStatus.Text = ""

            RichTextBox1.Text = Nothing
            NewVerifyTescoEsca()
            MsgBox(bb.QACertWhenKill(tb_Eartag.Text, lb_QAStatus, True, ProgressBar1, RichTextBox1))
            Me.ROIMoveTableAdapter.FillDataByEarTag(Me.AMPSLindenBeefDataSet.ROIMove, tb_Eartag.Text)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub bt_SearchAdd_Click(sender As Object, e As EventArgs) Handles bt_SearchAdd.Click
        lb_QAStatus.Text = ""
        RichTextBox1.Text = Nothing
        Dim fullBarcodes As String = tb_Barcodes1.Text & tb_Barcodes2.Text
        Dim len_fullBarcodes As Integer = fullBarcodes.Length
        Dim strFullBarcode As String = ""
        Dim EarTagBoolean As Boolean
        Dim recordsNo As Integer = 0


        'Dim strEarTag As String

        ' Try


        If len_fullBarcodes > 16 Then
                EarTagBoolean = False
            ElseIf len_fullBarcodes < 16 And len_fullBarcodes > 10 Then
                EarTagBoolean = True
            Else
                MsgBox("You must enter an eartag value to proceed")
                Exit Sub
            End If


        '**begin  Get the Eartag No 15/12/2015************

        If tb_Barcodes1.Text.StartsWith("IE") Then
            If tb_Barcodes1.Text.StartsWith("IE") Then
                strFullBarcode = tb_Barcodes1.Text.Substring(2)
            Else
                strFullBarcode = tb_Barcodes1.Text
            End If
            tb_Eartag.Text = "IE " & strFullBarcode.Substring(0, 2) & "-" & strFullBarcode.Substring(2, 5) & "-" & strFullBarcode.Substring(7, 1) &
                             "-" & strFullBarcode.Substring(8, 4)
            LabelEarTag.Text = tb_Eartag.Text.Replace("-", "").Replace(" ", "")


            'ElseIf tb_Barcodes1.Text.StartsWith("UK") Then
            '    strFullBarcode = tb_Barcodes1.Text
            '    tb_Eartag.Text = tb_Barcodes1.Text ' "UK " & strFullBarcode.Substring(0, 2) & "-" & strFullBarcode.Substring(2, 5) & "-" & strFullBarcode.Substring(7, 5)
            '    LabelEarTag.Text = tb_Barcodes1.Text '"UK" & strFullBarcode.Substring(0, 2) & strFullBarcode.Substring(2, 5) & strFullBarcode.Substring(7, 5)
        Else

            If tb_Barcodes1.Text.StartsWith("372") Then
                strFullBarcode = tb_Barcodes1.Text.Substring(3)
            Else
                strFullBarcode = tb_Barcodes1.Text
            End If
            tb_Eartag.Text = "372 " & strFullBarcode.Substring(0, 2) & "-" & strFullBarcode.Substring(2, 5) & "-" & strFullBarcode.Substring(7, 5)
            LabelEarTag.Text = "372" & strFullBarcode.Substring(0, 2) & strFullBarcode.Substring(2, 5) & strFullBarcode.Substring(7, 5)
        End If
        '*** end*********

        Me.ROIMoveTableAdapter.FillDataByEarTag(Me.AMPSLindenBeefDataSet.ROIMove, tb_Eartag.Text)

        '** begin  if recordsNo =0 then record is not yet add into ROIMoveEartag table and need to perform insert data,else display Animal details on fornt end
        recordsNo = CInt(getScalarStr("Select count(*) from ROIMoveEartag where eartag='" & tb_Eartag.Text & "'"))
        If recordsNo = 0 Then

            AddNewAnimalInfo(EarTagBoolean)
        Else
            LoadAnimalInfo(tb_Eartag.Text)
                MsgBox(bb.QACertWhenKill(tb_Eartag.Text, lb_QAStatus, False, ProgressBar1, RichTextBox1))
            End If
        '*** end***********
        'disable add new 29/03/2017
        '  enablebuttons()
        ' Catch ex As Exception
        '      MsgBox(ex.Message)
        ' End Try
    End Sub
    Private Sub LoadAnimalInfo(ByVal eartag As String)
        Using myconnection As New SqlConnection(My.Settings.LFKillConnection)
            myconnection.Open()
            Using mycommand As New SqlCommand("Select * from ROIMoveEartag where eartag='" & eartag & "'", myconnection)
        Using reader As SqlDataReader = mycommand.ExecuteReader
                    While reader.Read
                        tb_BirthHerdNo.Text = reader("BirthHerdno")
                        tb_moves.Text = reader("ROIMoves")
                        tb_NiMove.Text = reader("NIMoves")
                        dtp_DateOfBirth.Value = reader("DOB")
                        cb_ROIviaNI.Checked = reader("ROIViaNI")
                        dtp_PasswordRec.Value = reader("PassportReceived")
                        cb_Status.SelectedIndex = reader("PassportStatus")
                        'added on 29/03/2017
                        enablebuttons()
                    End While
                End Using
            End Using
        End Using

    End Sub
    Private Sub AddNewAnimalInfo(ByVal TF_eartag As Boolean)
        'data calculation and validation
        Dim ageInDays As Integer
        Dim str048 As String
        Dim HerdNoinKilltrans As String
        Dim intROIViaNI As Integer
        Dim strFQA As String = ""
        Dim intDaysOnLastFarm As Integer
        Dim intNIMoves As Integer
        Dim intMoves As Integer
        Dim intROIHerdID As Integer

        If TF_eartag = False Then
            dtp_DateOfBirth.Value = CDate(tb_Barcodes2.Text.Substring(0, 2) & "/" & tb_Barcodes2.Text.Substring(2, 2) & "/" & tb_Barcodes2.Text.Substring(4, 4))
        End If

        If getScalarStr("SELECT COUNT(killno) FROM Killtrans WHERE replace(replace(eartag,'-',''),' ','')  ='" & LabelEarTag.Text & "'") = 0 Then
            MsgBox("You must kill off this animal on APHIS before entering the details here")
            Exit Sub
        End If

        If tb_BirthHerdNo.Text = "" Or tb_BirthHerdNo.Text.Length < 6 Then
            MsgBox("You must enter a birth Herdno to proceed")
            Exit Sub
        End If

        If Not IsNumeric(tb_moves.Text) Then
            MsgBox("You must enter a number of moves to proceed")
            Exit Sub
        End If

        If (tb_moves.Text) < 1 Then
            MsgBox("You must enter a number of moves greater than 0 to proceed")
            Exit Sub
        End If

        If dtp_DateOfBirth.Value = Now.Date Then
            MsgBox("You must enter a valid date of birth to proceed")
            Exit Sub
        End If
        '*** begin data calculation for createing a new Record in ROIMoveEartag table
        ageInDays = DateDiff(DateInterval.Day, dtp_DateOfBirth.Value, Now.Date)
        If ageInDays < 1460 Then
            str048 = "N"
        Else
            str048 = "Y"
        End If

        HerdNoinKilltrans = getScalarStr("SELECT herd1 FROM Killtrans WHERE replace(replace(eartag,'-',''),' ','') ='" & LabelEarTag.Text & "'")
        If cb_ROIviaNI.Checked = True And HerdNoinKilltrans <> "" Then
            intROIViaNI = 1

            If getScalarStr("SELECT COUNT(killno) FROM Killtrans WHERE replace(replace(eartag,'-',''),' ','') ='" & LabelEarTag.Text & "'") = 1 Then
                tb_NiMove.Text = getScalarStr("SELECT origmoves -1  FROM Killtrans WHERE replace(replace(eartag,'-',''),' ','') ='" & LabelEarTag.Text & "'")
                strFQA = getScalarStr("SELECT origFQA FROM Killtrans WHERE replace(replace(eartag,'-',''),' ','') ='" & LabelEarTag.Text & "'")
                intDaysOnLastFarm = getScalarStr("SELECT DaysOnLastFarm FROM Killtrans WHERE replace(replace(eartag,'-',''),' ','') ='" & LabelEarTag.Text & "'")
            Else
                MsgBox("You must kill off this animal on APHIS before entering the details here")
                Exit Sub
            End If
        Else
            cb_ROIviaNI.Checked = False
            intROIViaNI = 0
            intNIMoves = 0
            tb_NiMove.Text = 0
        End If

        intMoves = CInt(tb_moves.Text) + intNIMoves
        'If intMoves = 1 Then
        '    runNonQuery("INSERT INTO RoiMoveEartag (Eartag, Killdate, BirthHerdNo, Moves, NIMoves, ROIMoves, DOB, AgeInDays,O48,ROIViaNI,BQAS,daysonlastfarm, lastupdated) VALUES('" & tb_Eartag.Text & "', GetDate()" &
        '           ", '" & tb_BirthHerdNo.Text & "', " & intMoves & ", " & intNIMoves & ", " & tb_moves.Text & ",'" & dtp_DateOfBirth.Value.Date.ToString("yyyy-MM-dd") & "'," & ageInDays & ", '" & str048 & "'," & intROIViaNI & ", '" & strFQA & "', " & intDaysOnLastFarm & ",GetDate())")
        'Else
        runNonQuery("INSERT INTO RoiMoveEartag (Eartag, Killdate, BirthHerdNo, Moves, NIMoves, ROIMoves, DOB, AgeInDays,O48,ROIViaNI,BQAS,daysonlastfarm, lastupdated) VALUES('" & tb_Eartag.Text & "', GetDate()" &
                  ", '" & tb_BirthHerdNo.Text & "', " & intMoves & ", " & intNIMoves & ", " & tb_moves.Text & ",'" & dtp_DateOfBirth.Value.Date.ToString("yyyy-MM-dd") & "'," & ageInDays & ", '" & str048 & "'," & intROIViaNI & ", '" & strFQA & "', " & intDaysOnLastFarm & ",GetDate())")



        '*** end

        '*** begin insert new record into Move table
        If getScalarStr("SELECT COUNT(*) FROM ROIMove WHERE eartag =  '" & tb_Eartag.Text & "'") = 0 Then

            If tb_moves.Text = 1 Then
                If getScalarStr("SELECT COUNT(*) FROM ROIMoveHerd WHERE herdno =  '" & tb_BirthHerdNo.Text & "'") = 0 Then

                    loadHerdDetails(tb_HerdNo.Text)
                    Me.Hide()
                    FormNewHerd.ShowDialog()
                    Me.Show()

                    If FormNewHerd.DialogResult <> DialogResult.OK Then
                        MsgBox("Update cancelled. You must enter a supplier as this animal was only on 1 farm")
                        Exit Sub
                    End If
                End If
                intROIHerdID = getScalarStr("SELECT ROIHerdID FROM ROIMoveHerd WHERE HerdNo = '" & tb_BirthHerdNo.Text & "'")
            Else
                intROIHerdID = 0
            End If

            runNonQuery("INSERT INTO ROIMove (Eartag,MoveDate,Herdno,ROIHerdID,FarmOfOrigin, DaysStayInFarm) VALUES('" & tb_Eartag.Text & "', '" & dtp_DateOfBirth.Value.Date.ToString("yyyy-MM-dd") & "', '" & tb_BirthHerdNo.Text &
                                           "', " & intROIHerdID & ", 1," & DateDiff(DateInterval.Day, dtp_DateOfBirth.Value, Date.Now) & ")")


            If tb_moves.Text = 1 Then
                '  bb.QACertWhenKill(tb_Eartag.Text, lb_QAStatus, True, ProgressBar1, RichTextBox1)        
                CallUpdate()
            End If
            '  runNonQuery("Update RoiMoveEartag set DaysOnLastFarm=" & DateDiff(DateInterval.Day, dtp_DateOfBirth.Value, Date.Now) & ", LastFarmHerdno='" & tb_BirthHerdNo.Text & "' where Eartag='" & tb_Eartag.Text & "'")


        End If
        '*** end
        Me.ROIMoveTableAdapter.FillDataByEarTag(Me.AMPSLindenBeefDataSet.ROIMove, tb_Eartag.Text)
        'added on 29/03/2017
        enablebuttons()
    End Sub

    Private Sub bt_NewSearch_Click(sender As Object, e As EventArgs) Handles bt_NewSearch.Click
        lb_QAStatus.Text = ""
        tb_Barcodes1.Text = ""
        tb_Barcodes2.Text = ""
        tb_Eartag.Text = ""
        LabelEarTag.Text = ""
        tb_BirthHerdNo.Text = ""
        tb_moves.Text = ""
        tb_HerdNo.Text = ""
        tb_NiMove.Text = 0
        cb_ROIviaNI.Checked = True
        dtp_DateOfBirth.Value = Now.Date
        dtp_PasswordRec.Value = Now.Date
        dtp_MoveDate.Value = Now.Date
        If dgv_Movements.Rows.Count > 0 Then
            Me.ROIMoveTableAdapter.FillDataByEarTag(Me.AMPSLindenBeefDataSet.ROIMove, tb_Eartag.Text)
            ' dgv_Movements.Rows.Clear()
            dgv_Movements.Refresh()

        End If

        tb_Barcodes1.Focus()
        RichTextBox1.Text = Nothing

        disablebuttons()
    End Sub

    Private Sub bt_Update_Click(sender As Object, e As EventArgs) Handles bt_Update.Click
        CallUpdate()
        'Try
        '    lb_QAStatus.Text = ""
        '    RichTextBox1.Text = Nothing
        '    Dim strSQL As String

        '    Dim intROIHerdID As Integer
        '    Dim ageInDays As Integer

        '    Dim intROIViaNI As Integer

        '    Dim strFQA As String
        '    Dim intOrigMoves As Integer
        '    Dim intOrigDaysOnLastFarm As Integer



        '    If tb_BirthHerdNo.Text = "" Or tb_BirthHerdNo.Text.Length < 6 Then
        '        MsgBox("You must enter a birth Herdno to proceed")
        '        Exit Sub
        '    End If

        '    If Not IsNumeric(tb_moves.Text) Then
        '        MsgBox("You must enter a number of moves to proceed")
        '        Exit Sub
        '    End If

        '    If (tb_moves.Text) < 1 Then
        '        MsgBox("You must enter a number of moves greater than 0 to proceed")
        '        Exit Sub
        '    End If

        '    If dtp_DateOfBirth.Value = Now.Date Then
        '        MsgBox("You must enter a valid date of birth to proceed")
        '        Exit Sub
        '    End If

        '    ageInDays = DateDiff(DateInterval.Day, dtp_DateOfBirth.Value, Now.Date)
        '    If cb_ROIviaNI.Checked Then
        '        intROIViaNI = 1

        '        If getScalarStr("SELECT COUNT(killno) FROM killtrans WHERE eartag = '" & tb_Eartag.Text & "'") = 0 Then
        '            MsgBox("The animal must be killed off on APHIS and downloaded before you can enter details for a nomad animal")
        '            Exit Sub
        '        End If

        '        strFQA = getScalarStr("SELECT origFQA FROM killtrans WHERE eartag = '" & tb_Eartag.Text & "'")
        '        tb_NiMove.Text = getScalarStr("SELECT origMoves -1 FROM killtrans WHERE eartag = '" & tb_Eartag.Text & "'")
        '        intOrigDaysOnLastFarm = getScalarStr("SELECT origDaysOnLastFarm FROM killtrans WHERE eartag = '" & tb_Eartag.Text & "'")
        '        runNonQuery("UPDATE ROIMoveEartag SET BQAS = '" & strFQA & "', NIMoves = " & intOrigMoves & ", daysOnLastFarm = " & intOrigDaysOnLastFarm & " WHERE eartag = '" & tb_Eartag.Text & "'")

        '    Else
        '        intROIViaNI = 0

        '    End If

        '    strSQL = "UPDATE  ROIMoveEartag SET BirthHerdNo = '" & tb_BirthHerdNo.Text & "',Moves= " & CInt(tb_moves.Text) + CInt(tb_NiMove.Text) & ", ROIMoves = " & tb_moves.Text & ",DOB = '" & dtp_DateOfBirth.Value.Date.ToString("yyyy-MM-dd") &
        '               "', ROIViaNi = " & intROIViaNI & ", ageindays = " & ageInDays & ", PassportReceived = '" & dtp_PasswordRec.Value.Date.ToString("yyyy-MM-dd") & "', PassportStatus = " & cb_Status.SelectedIndex &
        '               ", lastupdated = GetDate(), KillDate =GetDate() WHERE eartag =  '" & tb_Eartag.Text & "'"

        '    runNonQuery(strSQL)


        '    '**created 11/12/2015 cyang
        '    '**when update the DOB of a animal, the ROIMove table also need to updated to calculate the correct daysStayInFarm
        '    runNonQuery("Update ROIMove set MoveDate=" & dtp_DateOfBirth.Value & ",DaysStayInFarm=datediff(day,'" & dtp_DateOfBirth.Value.Date.ToString("yyyy-MM-dd") & "',DateMoveOffFarm) where Eartag='" & tb_Eartag.Text & "' and Herdno='" & tb_BirthHerdNo.Text & "'")
        '    '** end 

        '    If getScalarStr("SELECT COUNT(*) FROM ROIMove WHERE Herdno =  '" & tb_BirthHerdNo.Text & "'") = 0 Then
        '        If tb_moves.Text = 1 Then
        '            FormNewHerd.tb_HerdNo.Text = tb_BirthHerdNo.Text
        '            Me.Hide()
        '            FormNewHerd.ShowDialog()
        '            Me.Show()

        '            If FormNewHerd.DialogResult <> DialogResult.OK Then
        '                MsgBox("Update cancelled. You must enter a supplier as this animal was only on 1 farm")
        '                Exit Sub
        '            End If
        '            intROIHerdID = getScalarStr("SELECT ROIHerdID FROM ROIMoveHerd WHERE HerdNo = '" & tb_BirthHerdNo.Text & "'")
        '        Else
        '            intROIHerdID = 0
        '        End If
        '    End If
        '    runNonQuery("UPDATE ROIMove SET ROIHerdID = " & intROIHerdID & ", HerdNo = '" & tb_BirthHerdNo.Text & "',MoveDate = '" & dtp_DateOfBirth.Value.Date.ToString("yyyy-MM-dd") &
        '                           "' WHERE eartag = '" & tb_Eartag.Text & "' AND FarmOfOrigin =1")


        '    MsgBox(bb.QACertWhenKill(tb_Eartag.Text, lb_QAStatus, True, ProgressBar1, RichTextBox1))

        '    Me.ROIMoveTableAdapter.FillDataByEarTag(Me.AMPSLindenBeefDataSet.ROIMove, tb_Eartag.Text)

        '    If dgv_Movements.Rows.Count <> tb_moves.Text Then
        '        MsgBox("Warning you have entered an incorrect number of moves!!" & vbLf & "Verify that the quantity of moves = number of records in the movements grid")
        '    Else

        '        If (dtp_PasswordRec.Value = "1/1/1900" Or cb_Status.SelectedIndex = 0) And cb_ROIviaNI.Checked = True Then
        '            MsgBox("Check that the received date and receive status are correct")
        '        Else
        '            MsgBox("Update completed")
        '        End If

        '    End If

        '    NewVerifyTescoEsca()
        'Catch ex As Exception
        '    MsgBox("Error on Updating:" & ex.Message)
        'End Try
    End Sub
    Private Sub CallUpdate()
        Try
            lb_QAStatus.Text = ""
            RichTextBox1.Text = Nothing
            Dim strSQL As String

            Dim intROIHerdID As Integer
            Dim ageInDays As Integer

            Dim intROIViaNI As Integer

            Dim strFQA As String
            Dim intOrigMoves As Integer
            Dim intOrigDaysOnLastFarm As Integer



            If tb_BirthHerdNo.Text = "" Or tb_BirthHerdNo.Text.Length < 6 Then
                MsgBox("You must enter a birth Herdno to proceed")
                Exit Sub
            End If

            If Not IsNumeric(tb_moves.Text) Then
                MsgBox("You must enter a number of moves to proceed")
                Exit Sub
            End If

            Try
                If CInt(tb_moves.Text) < 1 Then
                    MsgBox("You must enter a number of moves greater than 0 to proceed")
                    Exit Sub
                End If
            Catch ex As Exception
                MsgBox("Value in Moves must be a number.")
                Exit Sub
            End Try


            If dtp_DateOfBirth.Value = Now.Date Then
                MsgBox("You must enter a valid date of birth to proceed")
                Exit Sub
            End If

            ageInDays = DateDiff(DateInterval.Day, dtp_DateOfBirth.Value.Date, Now.Date)
            If cb_ROIviaNI.Checked Then
                intROIViaNI = 1

                If getScalarStr("SELECT COUNT(killno) FROM killtrans WHERE TransDate>=getdate()-60 and replace(replace(eartag,'-',''),' ','') ='" & LabelEarTag.Text & "'") = 0 Then
                    MsgBox("The animal must be killed off on APHIS and downloaded before you can enter details for a nomad animal")
                    Exit Sub
                End If

                strFQA = getScalarStr("SELECT origFQA FROM killtrans WHERE TransDate>=getdate()-60 and replace(replace(eartag,'-',''),' ','') ='" & LabelEarTag.Text & "'")
                tb_NiMove.Text = getScalarStr("SELECT origMoves -1 FROM killtrans WHERE TransDate>=getdate()-60 and replace(replace(eartag,'-',''),' ','') ='" & LabelEarTag.Text & "'")
                intOrigDaysOnLastFarm = getScalarStr("SELECT origDaysOnLastFarm FROM killtrans WHERE TransDate>=getdate()-60 and replace(replace(eartag,'-',''),' ','') ='" & LabelEarTag.Text & "'")
                runNonQuery("UPDATE ROIMoveEartag SET BQAS = '" & strFQA & "', NIMoves = " & intOrigMoves & ", daysOnLastFarm = " & intOrigDaysOnLastFarm & " WHERE [DateTimeInserted]>=getdate()-60 and  eartag = '" & tb_Eartag.Text & "'")

            Else
                intROIViaNI = 0

            End If

            strSQL = "UPDATE  ROIMoveEartag SET BirthHerdNo = '" & tb_BirthHerdNo.Text & "',Moves= " & CInt(tb_moves.Text) + CInt(tb_NiMove.Text) & ", ROIMoves = " & tb_moves.Text & ",DOB = '" & dtp_DateOfBirth.Value.Date.ToString("yyyy-MM-dd") &
                       "', ROIViaNi = " & intROIViaNI & ", ageindays = " & ageInDays & ", PassportReceived = '" & dtp_PasswordRec.Value.Date.ToString("yyyy-MM-dd") & "', PassportStatus = " & cb_Status.SelectedIndex &
                       ", lastupdated = GetDate(), KillDate =GetDate() WHERE [DateTimeInserted]>=getdate()-60 and eartag =  '" & tb_Eartag.Text & "'"

            runNonQuery(strSQL)


            '**created 11/12/2015 cyang
            '**when update the DOB of a animal, the ROIMove table also need to updated to calculate the correct daysStayInFarm
            runNonQuery("Update ROIMove set MoveDate=" & dtp_DateOfBirth.Value & ",DaysStayInFarm=datediff(day,'" & dtp_DateOfBirth.Value.Date.ToString("yyyy-MM-dd") & "',DateMoveOffFarm) where [LastDateTimeUpdated]>=getdate()-60 and  Eartag='" & tb_Eartag.Text & "' and Herdno='" & tb_BirthHerdNo.Text & "'")
            '** end 

            If getScalarStr("SELECT COUNT(*) FROM ROIMove WHERE Herdno =  '" & tb_BirthHerdNo.Text & "'") = 0 Then
                If tb_moves.Text = 1 Then
                    FormNewHerd.tb_HerdNo.Text = tb_BirthHerdNo.Text
                    Me.Hide()
                    FormNewHerd.ShowDialog()
                    Me.Show()

                    If FormNewHerd.DialogResult <> DialogResult.OK Then
                        MsgBox("Update cancelled. You must enter a supplier as this animal was only on 1 farm")
                        Exit Sub
                    End If
                    intROIHerdID = getScalarStr("SELECT ROIHerdID FROM ROIMoveHerd WHERE HerdNo = '" & tb_BirthHerdNo.Text & "'")
                Else
                    intROIHerdID = 0
                End If
            End If
            runNonQuery("UPDATE ROIMove SET ROIHerdID = " & intROIHerdID & ", HerdNo = '" & tb_BirthHerdNo.Text & "',MoveDate = '" & dtp_DateOfBirth.Value.Date.ToString("yyyy-MM-dd") &
                                   "' WHERE eartag = '" & tb_Eartag.Text & "' AND FarmOfOrigin =1")


            MsgBox(bb.QACertWhenKill(tb_Eartag.Text, lb_QAStatus, True, ProgressBar1, RichTextBox1))

            Me.ROIMoveTableAdapter.FillDataByEarTag(Me.AMPSLindenBeefDataSet.ROIMove, tb_Eartag.Text)

            If dgv_Movements.Rows.Count <> tb_moves.Text Then
                MsgBox("Warning you have entered an incorrect number of moves!!" & vbLf & "Verify that the quantity of moves = number of records in the movements grid")
            Else

                If (dtp_PasswordRec.Value = "1/1/1900" Or cb_Status.SelectedIndex = 0) And cb_ROIviaNI.Checked = True Then
                    MsgBox("Check that the received date and receive status are correct")
                Else
                    MsgBox("Update completed")
                End If

            End If

            NewVerifyTescoEsca()
        Catch ex As Exception
            MsgBox("Error on Updating:" & ex.Message)
        End Try
    End Sub
    Private Sub DeleteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeleteToolStripMenuItem.Click
        Dim result As Integer = MessageBox.Show("Do you want to Delete the Selected Records", "Alert", MessageBoxButtons.OKCancel)
        If result = DialogResult.OK Then
            Try
                lb_QAStatus.Text = ""
                runNonQuery("DELETE FROM ROIMove WHERE eartag = '" & tb_Eartag.Text & "' AND FarmOfOrigin = 0")
                runNonQuery("Update ROIMove set DateMoveOffFarm=GetDate(), DaysStayInFarm=datediff(day,MoveDate,GetDate()) WHERE Eartag = '" & tb_Eartag.Text & "' AND FarmOfOrigin = 1")
                tb_HerdNo.Text = ""
                Me.ROIMoveTableAdapter.FillDataByEarTag(Me.AMPSLindenBeefDataSet.ROIMove, tb_Eartag.Text)
                MsgBox(bb.QACertWhenKill(tb_Eartag.Text, lb_QAStatus, True, ProgressBar1, RichTextBox1))
            Catch ex As Exception
                MsgBox("Errors on Deleting Movement: " & ex.Message)
            End Try
        End If

    End Sub

    Private Sub bt_AddMove_Click(sender As Object, e As EventArgs) Handles bt_AddMove.Click
        Try
            If dtp_MoveDate.Value.Date < dtp_DateOfBirth.Value.Date Then
                MsgBox("Move Date: " & dtp_MoveDate.Value.ToString("dd/MM/yyyy") & " is before the DOB, Please enter a correct Move Date!")
                Exit Sub
            End If
            If dtp_MoveDate.Value.Date >= Date.Now.Date Then
                MsgBox("Move Date: " & dtp_MoveDate.Value.ToString("dd/MM/yyyy") & " is on/after the KillDate, Please enter a correct Move Date!")
                Exit Sub
            End If


            For Each row As DataGridViewRow In dgv_Movements.Rows
                If dtp_MoveDate.Value.ToString("yyyy-MM-dd") = CDate(row.Cells(1).Value.ToString()).ToString("yyyy-MM-dd") Then
                    MsgBox("Move Date: " & dtp_MoveDate.Value.ToString("yyyy-MM-dd") & " has been used, please enter a correct Move Date!")
                    Exit Sub
                End If
            Next

            lb_QAStatus.Text = ""
            Dim intNumRecords As Integer
            Dim intROIHerdID As Integer

            intNumRecords = getScalarStr("SELECT COUNT(*) FROM ROIMove WHERE eartag = '" & tb_Eartag.Text & "'")
            If intNumRecords >= tb_moves.Text Then
                MsgBox("Number of moves already reached")
                Exit Sub
            End If

            If tb_HerdNo.Text.Length < 4 Then
                MsgBox("You must ener a valid herdno")
                Exit Sub
            End If

            If intNumRecords + 1 = tb_moves.Text Then
                intNumRecords = getScalarStr("SELECT COUNT(*) FROM ROIMoveHerd WHERE HerdNo = '" & tb_HerdNo.Text & "'")
                If intNumRecords = 0 Then

                    loadHerdDetails(tb_HerdNo.Text)
                    Me.Hide()
                    FormNewHerd.ShowDialog()
                    Me.Show()

                    If FormNewHerd.DialogResult <> DialogResult.OK Then
                        MsgBox("Herdno number move add cancelled")
                        Exit Sub
                    End If

                End If
                intROIHerdID = getScalarStr("SELECT ROIHerdID FROM ROIMoveHerd WHERE HerdNo = '" & tb_HerdNo.Text & "'")
            Else
                intROIHerdID = 0
            End If

            '**Created Date: 09/12/2015  By: cyang
            '**the purpose of runing below update is to get the DatemoveOffFarm and the days stay in Farm
            '  Dim temdate As String = dtp_MoveDate.Value.Year & "-" & dtp_MoveDate.Value.Month & "-" & dtp_MoveDate.Value.Day
            Dim temSQLQueryStr As String = "Update ROIMove set DateMoveOffFarm ='" & dtp_MoveDate.Value.Date.ToString("yyyy-MM-dd") & "', DaysStayInFarm=Datediff(day,MoveDate,'" & dtp_MoveDate.Value.Date.ToString("yyyy-MM-dd") & "')" &
                                             " where Eartag='" & tb_Eartag.Text & "' and ROIMoveID=(Select max(ROIMoveID) FROM ROIMove where Eartag='" & tb_Eartag.Text & "' and MoveDate =(SELECT Max(MoveDate) FROM ROIMove where Eartag='" & tb_Eartag.Text & "'))"
            runNonQuery(temSQLQueryStr)
            '** End

            '**Created Date: 11/12/2015  By: cyang
            '** blow sql insert is replace Jame's origin insert query to add the DaysStayInFarm
            Dim temDaysStayInFarm As Integer = DateDiff(DateInterval.Day, dtp_MoveDate.Value, Now.Date)
            runNonQuery("INSERT INTO ROIMove (Eartag,MoveDate,Herdno,ROIHerdID,DaysStayInfarm) VALUES ('" & tb_Eartag.Text & "','" & dtp_MoveDate.Value.Date.ToString("yyyy-MM-dd") & "', '" & tb_HerdNo.Text & "', " & intROIHerdID & "," & temDaysStayInFarm & ")")
            ' runNonQuery("Update ROIMoveEartag set DaysOnLastFarm=" & temDaysStayInFarm & ", lastfarmherdno='" & tb_HerdNo.Text & "', LastUpdated=GetDate()  where Eartag='" & tb_Eartag.Text & "'")
            '** end
            Me.ROIMoveTableAdapter.FillDataByEarTag(Me.AMPSLindenBeefDataSet.ROIMove, tb_Eartag.Text)
            tb_HerdNo.Text = ""

            If dgv_Movements.RowCount = tb_moves.Text Then
                NewVerifyTescoEsca()
                ' MsgBox(bb.QACertWhenKill(tb_Eartag.Text, lb_QAStatus, True, ProgressBar1, RichTextBox1))
                CallUpdate()
            End If
        Catch ex As Exception
            MsgBox("Error on Add Movement:" & ex.Message)
        End Try
    End Sub

    Private Sub bt_EditHerd_Click(sender As Object, e As EventArgs) Handles bt_EditHerd.Click
        lb_QAStatus.Text = ""
        If tb_HerdNo.Text.Length > 4 Then


            loadHerdDetails(tb_HerdNo.Text)
            Me.Hide()
            FormNewHerd.ShowDialog()
            Me.Show()


            'Me.Hide()
            'loadHerdDetails(tb_HerdNo.Text)
            'FormNewHerd.Show()
        Else
            MsgBox("You must ener a valid herdno")
            Exit Sub
        End If
    End Sub

    Sub disablebuttons()
        bt_AddMove.Enabled = False
        bt_EditHerd.Enabled = False
        bt_SearchAdd.Enabled = False
        bt_Update.Enabled = False
        bt_NewSearch.Enabled = False
        bt_CheckSQStatus.Enabled = False
    End Sub
    Sub enablebuttons()
        bt_AddMove.Enabled = True
        bt_EditHerd.Enabled = True
        bt_SearchAdd.Enabled = True
        bt_Update.Enabled = True
        bt_NewSearch.Enabled = True
        bt_CheckSQStatus.Enabled = True
    End Sub
    Sub enableSearchAddButton()
        bt_SearchAdd.Enabled = True
    End Sub
    Sub disableSearchAddButton()
        bt_SearchAdd.Enabled = False
    End Sub

    Private Sub tb_Barcodes1_TextChanged(sender As Object, e As EventArgs) Handles tb_Barcodes1.TextChanged
        'added on  29/03/2017
        If tb_Barcodes1 IsNot String.Empty And tb_Barcodes2.Text IsNot String.Empty And tb_moves.Text IsNot String.Empty And tb_HerdNo.Text IsNot String.Empty Then
            enableSearchAddButton()
        Else
            disableSearchAddButton()
        End If
        'disable on 29/03/2017
        'If tb_Barcodes1.Text <> "" And tb_Barcodes2.Text <> "" Then
        '    enableSearchAddButton()
        'Else
        '    disableSearchAddButton()
        'End If

        If tb_Barcodes1.Text <> "" Or tb_Barcodes2.Text <> "" Then
            bt_NewSearch.Enabled = True
        Else
            bt_NewSearch.Enabled = False
        End If


    End Sub

    Private Sub tb_Barcodes2_TextChanged(sender As Object, e As EventArgs) Handles tb_Barcodes2.TextChanged
        If tb_Barcodes1 IsNot String.Empty And tb_Barcodes2.Text IsNot String.Empty And tb_moves.Text IsNot String.Empty And tb_BirthHerdNo.Text IsNot String.Empty Then
            enableSearchAddButton()
        Else
            disableSearchAddButton()
        End If
        If tb_Barcodes1.Text <> "" Or tb_Barcodes2.Text <> "" Then
            bt_NewSearch.Enabled = True
        Else
            bt_NewSearch.Enabled = False
        End If
        If tb_Barcodes2.Text.Length >= 8 Then
            dtp_DateOfBirth.Value = tb_Barcodes2.Text.Substring(4, 4) & "-" & tb_Barcodes2.Text.Substring(2, 2) & "-" & tb_Barcodes2.Text.Substring(0, 2)
        End If

    End Sub


    Sub loadHerdDetails(ByVal herdno As String)

        FormNewHerd.tb_HerdNo.Text = herdno
        Using myconnection As New SqlConnection(My.Settings.LFKillConnection)
            myconnection.Open()
            Using mycommand As New SqlCommand("SELECT ROIHerdID,isnull(Name,'') as Name, isnull(TownLand,'') as Town, isnull(County,'') as County FROM ROIMoveHerd WHERE herdno = '" & herdno & "'", myconnection)
                Using reader As SqlDataReader = mycommand.ExecuteReader
                    If reader.HasRows Then
                        FormNewHerd.isHerdExist = True
                        While reader.Read

                            FormNewHerd.ROIHerdID = CInt(reader("ROIHerdID"))
                            FormNewHerd.tb_Name.Text = reader("Name")
                            FormNewHerd.tb_Town.Text = reader("Town")
                            FormNewHerd.tb_County.Text = reader("County")
                        End While
                    Else
                        FormNewHerd.isHerdExist = False
                    End If
                End Using
            End Using
        End Using

    End Sub

    Private Sub NewVerifyTescoEsca()

        Dim intROIMoves As Integer
        Dim intCountOfRecords As Integer
        Dim strUpdate As String

        If getScalarStr("SELECT count(*) FROM ROIMoveEartag WHERE Eartag= '" & tb_Eartag.Text & "'") = 1 Then
            intCountOfRecords = getScalarStr("SELECT COUNT(*) FROM ROIMove WHERE Eartag = '" & tb_Eartag.Text & "'")
            intROIMoves = getScalarStr("SELECT ROIMoves FROM ROIMoveEartag WHERE Eartag = '" & tb_Eartag.Text & "'")
            If intCountOfRecords = intROIMoves Then
                strUpdate = "UPDATE ROIMoveEartag SET Status = 5 WHERE Eartag = '" & tb_Eartag.Text & "'"
            Else
                strUpdate = "UPDATE ROIMoveEartag SET Status = 0 WHERE Eartag = '" & tb_Eartag.Text & "'"
            End If

            runNonQuery(strUpdate)
        End If

    End Sub
    Public Sub ExportExcel()
        Dim path As String = My.Computer.FileSystem.SpecialDirectories.MyDocuments & "\ROIMovements\" & dtp_DownloadDatabyDate.Value.Date.ToString("dd_MM_yyyy") & ".xlsx"
        Dim folder As String = My.Computer.FileSystem.SpecialDirectories.MyDocuments & "\ROIMovements\"

        If Not Directory.Exists(folder) Then
            Directory.CreateDirectory(folder)
        End If
        Try
            If File.Exists(path) Then
                File.Delete(path)
            End If
        Catch ex As Exception
            MsgBox("File " & dtp_DownloadDatabyDate.Value.Date.ToString("dd_MM_yyyy") & ".xlsx" & " is opened, Please close it first!")
            'Exit Sub
        End Try



        Dim xlApp As Excel.Application
        Dim xlWorkBook As Excel.Workbook
        Dim xlWorkSheet As Excel.Worksheet
        Dim misValue As Object = System.Reflection.Missing.Value
        Dim i As Integer = 1
        Dim j As Integer = 0

        xlApp = New Excel.Application
        xlWorkBook = xlApp.Workbooks.Add(misValue)
        xlWorkSheet = CType(xlWorkBook.Sheets("sheet1"), Excel.Worksheet)

        xlWorkSheet.Cells(i, 1) = "Tag Number"
        xlWorkSheet.Cells(i, 2) = "Herd Number"
        xlWorkSheet.Cells(i, 3) = "Date of Movement Off Farm"
        xlWorkSheet.Cells(i, 4) = "Status"
        Using myconnection As New SqlConnection(My.Settings.LFKillConnection)
            myconnection.Open()
            Using mycommand As New SqlCommand("Select isnull(Eartag,'') as Eartag, isnull(Herdno,'') as Herdno,  convert(varchar,DateMoveoffFarm,103) as DateMoveoffFarm FROM [AMPSLindenBeef].[dbo].[ROIMove] where left(convert(varchar,[InsertDateTime],121),10)='" & dtp_DownloadDatabyDate.Value.Date.ToString("yyyy-MM-dd") & "'", myconnection)
                Using reader As SqlDataReader = mycommand.ExecuteReader
                    If reader.HasRows Then
                        While reader.Read
                            i = i + 1
                            xlWorkSheet.Cells(i, 1) = reader("Eartag").ToString
                            xlWorkSheet.Cells(i, 2) = reader("Herdno").ToString
                            xlWorkSheet.Cells(i, 3) = CDate(reader("DateMoveoffFarm"))
                        End While
                    Else
                        MsgBox("There is no record to be download on " & dtp_DownloadDatabyDate.Value.Date.ToString("dd/MM/yyyy"))
                        Exit Sub
                    End If
                End Using
            End Using
        End Using


        'For i = 0 To gv.RowCount - 2
        '    For j = 0 To gv.ColumnCount - 1
        '        If gv(j, i).Value IsNot Nothing Then
        '            xlWorkSheet.Cells(i + 1, j + 1) =
        '                gv(j, i).Value.ToString()
        '        End If
        '    Next

        'Next
        xlWorkSheet.Name = "Batch Residency Check"
        xlWorkSheet.SaveAs(path)
        xlWorkBook.Close()

        xlApp.Quit()
        releaseObject(xlApp)
        releaseObject(xlWorkBook)
        releaseObject(xlWorkSheet)
        MsgBox("Animal details have been exported to" & vbCrLf & path)

        'Dim result As Integer = MessageBox.Show("Animal details have been exported to" & vbCrLf & path & vbCrLf & "Would you like to open this file?", "Export Alert", MessageBoxButtons.YesNo)
        'If result = DialogResult.Yes Then
        '    MsgBox("Hello")
        '    Dim oExcel As Object
        '    oExcel = CreateObject("Excel.Application")
        '    oExcel.workbooks.Open(path)

        'End If


    End Sub
    Private Sub releaseObject(ByVal obj As Object)
        Try
            If obj IsNot Nothing Then
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
                obj = Nothing
            End If

        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub



    Private Sub tb_AnimalFQAResultKeywords_TextChanged(sender As Object, e As EventArgs) Handles tb_AnimalFQAResultKeywords.TextChanged
        If tb_AnimalFQAResultKeywords.Text <> "" Then
            Me.AnimalQAStatusTableAdapter.FillByKeystr(Me.AMPSLindenBeefDataSet.AnimalQAStatus, "%" & tb_AnimalFQAResultKeywords.Text & "%")
        Else
            Me.AnimalQAStatusTableAdapter.Fill(Me.AMPSLindenBeefDataSet.AnimalQAStatus)
        End If
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles bt_Download.Click
        ExportExcel()
        'Using myconnection As New SqlConnection(My.Settings.LFKillConnection)
        '    myconnection.Open()
        '    Using mycommand As New SqlCommand("SELECT distinct [Eartag] FROM [AMPSLindenBeef].[dbo].[ROIMove] where [InsertDateTime]>'2015-12-17' and Eartag not in (Select EarTag from ROIAnimalQAStatus)", myconnection)
        '        Using reader As SqlDataReader = mycommand.ExecuteReader

        '            While reader.Read
        '                '  MsgBox(reader("Eartag"))
        '                bb.QACertWhenKill(reader("Eartag"), lb_QAStatus, True, ProgressBar1, RichTextBox1)

        '            End While
        '        End Using
        '    End Using
        'End Using
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked

        System.Diagnostics.Process.Start("https://qas.bordbia.ie/Beef/Meat_Plant/Residency_Check.aspx?MP")
    End Sub

    Private Sub tb_HerdNo_TextChanged(sender As Object, e As EventArgs) Handles tb_HerdNo.TextChanged
        If isHerdDetailExist(tb_HerdNo.Text.Trim) Then
            bt_AddMove.Enabled = True
        Else
            bt_AddMove.Enabled = False
            If Trim(tb_HerdNo.Text).Length >= 8 Then
                FormNewHerd.Show()
                FormNewHerd.tb_HerdNo.Text = tb_HerdNo.Text
                Me.Hide()
            End If
        End If
    End Sub

    Function isHerdDetailExist(ByVal HerdNo As String) As Boolean
        Dim tf As Boolean = False
        Using myconnection As New SqlConnection(My.Settings.LFKillConnection)
            myconnection.Open()
            Using mycommand As New SqlCommand("Select * from ROIMoveHerd where HerdNo='" & HerdNo & "'", myconnection)
                Using reader As SqlDataReader = mycommand.ExecuteReader
                    If reader.HasRows Then
                        tf = True
                    End If
                End Using
            End Using
        End Using

        Return tf
    End Function

    Private Sub TextBox_UKCattleEartag_TextChanged(sender As Object, e As EventArgs) Handles TextBox_UKCattleEartag.TextChanged

        Select Case TextBox_UKCattleEartag.Text.Length
            Case 2
                If TextBox_UKCattleEartag.Text = "IE" Then
                    MsgBox("This is not a UK Cattle, please confirm yor Eartag")
                    Exit Sub
                End If
            Case 3
                If TextBox_UKCattleEartag.Text = "UK9" Then
                    MsgBox("This is a NI Cattle, Plase confirm your Eartag")
                    Exit Sub
                End If
            Case Is = 15
                Using myconnection As New SqlConnection(My.Settings.LFKillConnection)
                    myconnection.Open()
                    Using mycommand As New SqlCommand("Select count(*) as No from KILLTRANS WHERE eartag='" & TextBox_UKCattleEartag.Text & "'", myconnection)
                        Using reader As SqlDataReader = mycommand.ExecuteReader

                            While reader.Read
                                If reader("No") = 1 Then
                                    MsgBox(reader("No") & " animal(s) found")
                                    Button_UpdateUKCattleMoves.Enabled = True
                                Else
                                    MsgBox("There is no animal matched")
                                    Button_UpdateUKCattleMoves.Enabled = False
                                End If

                            End While

                        End Using
                    End Using
                End Using
        End Select
    End Sub

    Private Sub Button_UpdateUKCattleMoves_Click(sender As Object, e As EventArgs) Handles Button_UpdateUKCattleMoves.Click
        Try
            Using myconnection As New SqlConnection(My.Settings.LFKillConnection)
                myconnection.Open()
                Using mycommand As New SqlCommand("Select count(*) as No from KILLTRANS WHERE eartag='" & TextBox_UKCattleEartag.Text & "' and transtime is null and sex=''", myconnection)
                    Using reader As SqlDataReader = mycommand.ExecuteReader

                        While reader.Read
                            If reader("No") = "1" Then
                                runNonQuery("Update KILLTRANS Set Moves=" & TextBox_UKCattleMoveNo.Text & " where eartag='" & TextBox_UKCattleEartag.Text & "' and transtime is null and sex=''")
                                MultiflexrunNonQuery("Update Beef_AphisData_ANL Set NumberOfFarmMove=" & TextBox_UKCattleMoveNo.Text & " where animalTagNumber='" & TextBox_UKCattleEartag.Text & "'")
                                MsgBox("Animal is Updated!")
                                '' MsgBox(reader("No") & " animal(s) found")
                                ''Button_UpdateUKCattleMoves.Enabled = True
                            Else
                                MsgBox("This update should be taken before Hide Puller!")
                                '' Button_UpdateUKCattleMoves.Enabled = False
                            End If

                        End While

                    End Using
                End Using
            End Using


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub TextBox_UKCattleMoveNo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox_UKCattleMoveNo.KeyPress
        If Asc(e.KeyChar) <> 13 AndAlso Asc(e.KeyChar) <> 8 AndAlso Not IsNumeric(e.KeyChar) Then
            MessageBox.Show("Please enter numbers only")
            e.Handled = True
        End If
    End Sub

    Private Sub tb_BirthHerdNo_TextChanged(sender As Object, e As EventArgs) Handles tb_BirthHerdNo.TextChanged
        If tb_Barcodes1 IsNot String.Empty And tb_Barcodes2.Text IsNot String.Empty And tb_moves.Text IsNot String.Empty And tb_BirthHerdNo.Text IsNot String.Empty Then
            enableSearchAddButton()
        Else
            disableSearchAddButton()
        End If

        'added 27/03/2017 to validate the birth herdno
        If isHerdDetailExist(tb_BirthHerdNo.Text.Trim) = False Then
            bt_Update.Enabled = False
            If Trim(tb_HerdNo.Text).Length >= 8 Then
                FormNewHerd.Show()
                FormNewHerd.tb_HerdNo.Text = tb_BirthHerdNo.Text
                Me.Hide()
            End If
        End If
    End Sub

    Private Sub tb_moves_TextChanged(sender As Object, e As EventArgs) Handles tb_moves.TextChanged
        If tb_Barcodes1 IsNot String.Empty And tb_Barcodes2.Text IsNot String.Empty And tb_moves.Text IsNot String.Empty And tb_BirthHerdNo.Text IsNot String.Empty Then
            enableSearchAddButton()
        Else
            disableSearchAddButton()
        End If
    End Sub

    Private Sub tb_Eartag_TextChanged(sender As Object, e As EventArgs) Handles tb_Eartag.TextChanged
        LabelEarTag.Text = tb_Eartag.Text
    End Sub



    'Private Sub TextBox_Downgrade_Eartag_TextChanged(sender As Object, e As EventArgs)
    '    If TextBox_Downgrade_Eartag.Text.Length >= 14 Then
    '        Dim da As SqlClient.SqlDataAdapter
    '        Dim ds As New DataSet

    '        Dim sqlstr As String = "Select Sex,convert(int,[Months]) as Months,convert(varchar(10),DateBirth,103) as DateBirth,FQA,EarTag from KILLTRANS WHERE replace(eartag,' ','')='" & TextBox_Downgrade_Eartag.Text.Trim.Replace(" ", "") & "'"
    '        da = New SqlClient.SqlDataAdapter(sqlstr, My.Settings.LFKillConnection)
    '        da.Fill(ds, "Killtran")
    '        da.Dispose()

    '        If ds.Tables(0).Rows.Count = 1 Then
    '            TextBox_DownGrade_Month.Text = ds.Tables(0).Rows(0).Item("Months")
    '            TextBox_DownGrade_Sex.Text = ds.Tables(0).Rows(0).Item("Sex")
    '            'TextBox_DOB.Text = ds.Tables(0).Rows(0).Item("DateBirth")
    '            DateTimePicker_DownGrade_DOB.Value = ds.Tables(0).Rows(0).Item("DateBirth")
    '            TextBox_Downgrade_Eartag.Text = ds.Tables(0).Rows(0).Item("EarTag")
    '            If ds.Tables(0).Rows(0).Item("FQA") = "Q" Then
    '                CheckBox_DownGrade_Yes.Checked = True
    '                CheckBox_DownGrade_No.Checked = False
    '            Else
    '                CheckBox_DownGrade_Yes.Checked = False
    '                CheckBox_DownGrade_No.Checked = True
    '            End If
    '        Else
    '            MsgBox("Animal cannot be found in system, please check your EarTag.")
    '        End If


    '    End If


    'End Sub

    'Private Sub Button_DownGrade_Update_Click(sender As Object, e As EventArgs)
    '    Dim da As SqlClient.SqlDataAdapter
    '    Dim ds As New DataSet

    '    Dim sqlstr As String = "Select Sex,convert(int,[Months]) as Months,convert(varchar(10),DateBirth,103) as DateBirth,FQA,EarTag from KILLTRANS WHERE replace(eartag,' ','')='" & TextBox_Downgrade_Eartag.Text.Trim.Replace(" ", "") & "'"
    '    da = New SqlClient.SqlDataAdapter(sqlstr, My.Settings.LFKillConnection)
    '    da.Fill(ds, "Killtran")
    '    da.Dispose()
    '    If ds.Tables(0).Rows.Count = 1 Then
    '        Dim FQAStr As String = "Q"
    '        If CheckBox_DownGrade_No.Checked Then
    '            FQAStr = "N"
    '        End If

    '        runNonQuery("update KILLTRANS set Sex='" & TextBox_DownGrade_Sex.Text.Trim & "',FQA='" & FQAStr & "', Months=" & TextBox_DownGrade_Month.Text & ", DateBirth='" & DateTimePicker_DownGrade_DOB.ToString("yyyy-MM-dd") & "' where eartag='" & TextBox_Downgrade_Eartag.Text.Trim & "'")

    '        Dim AphisMonths As Integer = CInt(TextBox_DownGrade_Month.Text)
    '        Dim Over24Months As String = "N"
    '        Dim Over30Months As String = "N"
    '        Dim Over48Months As String = "N"
    '        Dim AphisFQA As String = "Y"
    '        If CheckBox_DownGrade_No.Checked Then
    '            FQAStr = "N"
    '        End If

    '        Select Case AphisMonths
    '            Case 24 To 29
    '                Over24Months = "Y"
    '            Case 30 To 47
    '                Over24Months = "Y"
    '                Over30Months = "Y"
    '            Case >= 48
    '                Over24Months = "Y"
    '                Over30Months = "Y"
    '                Over48Months = "Y"
    '        End Select

    '        MultiflexrunNonQuery("Update Beef_AphisData_ANL set Sex='" & TextBox_DownGrade_Sex.Text.Trim & "', FQAS='" & FQAStr & "', Months=" & TextBox_DownGrade_Month.Text & ", DateOfBirth='" & DateTimePicker_DownGrade_DOB.ToString("yyyy-MM-dd") & "', Over24Months='" & Over24Months & "', Over30Months='" & Over30Months & "', Over48Months='" & Over48Months & "' where AnimalTagNumber='" & TextBox_Downgrade_Eartag.Text.Trim & "'")

    '    Else
    '        MsgBox("Animal cannot be found in system, please check your EarTag.")
    '    End If

    'End Sub




    'Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
    '    Using myconnection As New SqlConnection(My.Settings.LFKillConnection)
    '        myconnection.Open()
    '        Using mycommand As New SqlCommand("Select  distinct Eartag from ROIMove WHERE  (LastDateTimeUpdated >= '2016-01-07') AND (StatusLog IS NULL)", myconnection)
    '            Using reader As SqlDataReader = mycommand.ExecuteReader
    '                While reader.Read
    '                    bb.QACertWhenKill(reader("Eartag"), lb_QAStatus, True, ProgressBar1, RichTextBox1)
    '                    'If bb.CheckQAStatus(reader("Herdno").ToString.Trim, CDate(reader("DateMoveOffFarm"))) Then
    '                    '    runNonQuery("Update ROIMove set HerdStatus='T' where ROIMoveID=" & reader("ROIMoveID").ToString.Trim)
    '                    'Else
    '                    '    runNonQuery("Update ROIMove set HerdStatus='F' where ROIMoveID=" & reader("ROIMoveID").ToString.Trim)
    '                    'End If
    '                End While
    '            End Using
    '        End Using
    '    End Using
    '    '  Dim temdate As String = dtp_MoveDate.Value.Year & "-" & dtp_MoveDate.Value.Month & "-" & dtp_MoveDate.Value.Day

    'End Sub


End Class
