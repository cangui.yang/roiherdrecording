﻿Imports System.Data.SqlClient
Imports System.Math

Public Class BordBia

    Function CheckNetworkStatus() As Boolean
        Dim tf As Boolean = False
        If My.Computer.Network.IsAvailable = False Then
            MsgBox("Connection Issue: Please Check your Internet Connection or Bord Bia web site.")
            tf = False
        Else
            ' MsgBox("Connected")
            If CheckBordBiaConnection() Then
                tf = True
            Else
                MsgBox("Fail to connect to Bord Bia Webservices, please check your network.")
                tf = False
            End If


        End If
        Return tf
    End Function
    Function CheckBordBiaConnection() As Boolean
        Dim req As System.Net.HttpWebRequest
        Dim res As System.Net.HttpWebResponse
        CheckBordBiaConnection = False
        Try
            req = CType(System.Net.HttpWebRequest.Create("https://qas.bordbia.ie/Beef/CheckHerdStatus.asmx"), System.Net.HttpWebRequest)
            res = CType(req.GetResponse(), System.Net.HttpWebResponse)
            req.Abort()
            If res.StatusCode = System.Net.HttpStatusCode.OK Then
                CheckBordBiaConnection = True
            End If
        Catch weberrt As System.Net.WebException
            CheckBordBiaConnection = False
        Catch except As Exception
            CheckBordBiaConnection = False
        End Try

    End Function


    'Created Date: 09/12/2015  By: cyang
    'Validate whether a animal is in QA cert when get kill in Linden Foods
    Function QACertWhenKill(ByVal EarTag As String, ByVal ReturnMessage As Label, ByVal checkStatus As Boolean, ByVal progressbar As ProgressBar, ByVal rtb As RichTextBox) As String
        If CheckNetworkStatus() = False Then
            Return "Network Errors"
            Exit Function
        End If
        Dim tem As String = ""
        Dim QACert As String = "Not QA Cert"
        Dim LogTxt As String = "Last 70 days Not In QA"
        Dim lastFarmNo As String = ""
        Dim daysonLastFarm As String = ""
        ReturnMessage.ForeColor = Color.Red
        Dim QADays As Integer = 0
        rtb.Text = Nothing
        If checkStatus Then
            NewBQASStatus(EarTag, progressbar, rtb)
            ' MsgBox(1)
        End If
        '    Try
        'Using SqlConnection As New SqlConnection(My.Settings.LFKillConnection)
        '    SqlConnection.Open()
        '    Using sqlcomm As New SqlCommand("Select BirthHerdno,Moves from RoiMoveEartag where Eartag='" & EarTag & "'", SqlConnection)
        '        Using reader As SqlDataReader = sqlcomm.ExecuteReader
        '            While reader.Read
        '                If reader("Moves") = "1" Then
        '                    lastFarmNo = reader("BirthHerdno")
        '                End If
        '            End While
        '        End Using

        '    End Using
        'End Using



        Using SqlConnection As New SqlConnection(My.Settings.LFKillConnection)
                SqlConnection.Open()
                Using sqlcomm As New SqlCommand("Select HerdStatus, HerdNo, inQADays,DaysStayInFarm from ROIMove where Eartag='" & EarTag & "' order by MoveDate Desc", SqlConnection)
                    Using reader As SqlDataReader = sqlcomm.ExecuteReader
                        While reader.Read

                            If reader("HerdStatus") = "T" Then
                                QADays = QADays + CInt(reader("DaysStayInFarm"))

                                If QADays > 70 Then
                                    AppendTextBox(rtb, Environment.NewLine)
                                    AppendTextBox(rtb, "Animal Status: FQA")
                                    AppendTextBox(rtb, Environment.NewLine)
                                    AppendTextBox(rtb, "Log: Last 70 days In QA")

                                    Exit While
                                End If
                            Else

                                AppendTextBox(rtb, "Animal Status: Not FQA")
                                AppendTextBox(rtb, Environment.NewLine)
                                AppendTextBox(rtb, "Log: Last 70 days Not In QA")
                                Exit While
                            End If
                        End While
                    End Using
                End Using
            End Using
            Using SqlConnection As New SqlConnection(My.Settings.LFKillConnection)
                SqlConnection.Open()
                Using sqlcomm As New SqlCommand("Select top 1 HerdNo, DaysStayInFarm from ROIMove where Eartag='" & EarTag & "' order by MoveDate Desc", SqlConnection)
                    Using reader As SqlDataReader = sqlcomm.ExecuteReader
                        While reader.Read
                            lastFarmNo = reader("HerdNo")
                            daysonLastFarm = reader("DaysStayInFarm")
                        End While
                    End Using
                End Using
            End Using
            If QADays >= 70 Then
                QACert = "QA Cert"
                LogTxt = "Last 70 days In QA"
                ReturnMessage.ForeColor = Color.Green
                tem = "Q"
            End If
            If getScalarStr("Select count(*) from ROIAnimalQAStatus where EarTag='" & EarTag & "'") > 0 Then
                runNonQuery("Update ROIAnimalQAStatus Set QAStatus='" & QACert & "', Log='" & LogTxt & "',LastUpdateDateTime=GetDate() where EarTag='" & EarTag & "'")
            Else
                runNonQuery("insert into ROIAnimalQAStatus (EarTag,QAStatus,Log) values('" & EarTag & "','" & QACert & "','" & LogTxt & "')")
            End If

        UpdateMoveEartag(tem, daysonLastFarm, EarTag, lastFarmNo)
        UpdateKillLine(daysonLastFarm, EarTag)
        ReturnMessage.Text = QACert

            'Catch ex As Exception
            '    MsgBox("Error in QACertWhenKill:" & ex.Message)
            'End Try


            Return QACert

    End Function

    'Created Date: 09/12/2015  By: cyang
    'the purpose of creating this new function is to replace the getBQASStatus, and validate whether is QA Cert in Each Movement'  , 
    Private Sub NewBQASStatus(ByVal eartag As String, ByVal progressbar As ProgressBar, ByVal rtb As RichTextBox)
        Dim BQASLookup As New ie.bordbia.qas.CheckHerdStatus
        Dim QAvalue As Integer = 0
        '  Dim strResult As String
        Dim DateMoveInFarm As Date
        Dim DateMoveOffFarm As Date
        Dim HerdNo As String
        Dim DaysStayinFarm As Integer
        Dim DaysInQA As Integer
        'QA in Herd Level
        Try
            rtb.Text = Nothing
            progressbar.Visible = True
            progressbar.Minimum = 0
            progressbar.Maximum = getScalarStr("Select Count(*) from ROIMove where Eartag='" & eartag & "'") * 2
            progressbar.Value = 0
            progressbar.Step = 1
            Using SqlConnection As New SqlConnection(My.Settings.LFKillConnection)
                SqlConnection.Open()
                Using sqlcomm As New SqlCommand("Select isnull(HerdNo,'')as HerdNo, isnull(DaysStayInFarm,0) as DaysStayInFarm, isnull(convert(varchar,MoveDate,103),'') as DateMoveInFarm,isnull(convert(varchar,DateMoveOffFarm,103),'') as DateMoveOffFarm,isnull(convert(varchar,DateMoveOffFarm-70,103),'') as Last70DaysInFarm from ROIMove where Eartag='" & eartag & "'", SqlConnection)
                    Using reader As SqlDataReader = sqlcomm.ExecuteReader
                        While reader.Read
                            ' if DateMoveInFarm is QA

                            HerdNo = reader("HerdNo")
                            '     MsgBox(HerdNo)
                            DateMoveInFarm = CDate(reader("DateMoveInFarm"))
                            DateMoveOffFarm = CDate(reader("DateMoveOffFarm"))
                            DaysStayinFarm = CInt(reader("DaysStayInFarm"))
                            DaysInQA = 0
                            progressbar.PerformStep()
                            AppendTextBox(rtb, "HerdNo.:" & HerdNo)
                            AppendTextBox(rtb, Environment.NewLine)
                            AppendTextBox(rtb, "------------------")
                            AppendTextBox(rtb, Environment.NewLine)
                            'MsgBox("check MoveInDate:" & HerdNo & "-" & DateMoveInFarm)
                            ' MsgBox(1)
                            If CheckQAStatus(HerdNo, DateMoveInFarm) Then
                                'if DateMoveOffFarm is QA
                                'MsgBox("check MoveOffDate1:" & HerdNo & "-" & DateMoveOffFarm)
                                ' -- MsgBox(2)
                                If CheckQAStatus(HerdNo, DateMoveOffFarm) Then
                                    '       MsgBox("3 T in QA")
                                    QALog("T", "in QA", DaysInQA, DaysStayinFarm, eartag, HerdNo)
                                    AppendTextBox(rtb, "Herd Status: FQA")
                                    AppendTextBox(rtb, Environment.NewLine)
                                Else
                                    QAvalue = QAvalue + 1
                                    '       MsgBox("4 F, Move in date in QA, but move off date not in QA")
                                    QALog("F", "Move in date in QA, but move off date not in QA", DaysInQA, DaysStayinFarm, eartag, HerdNo)
                                    AppendTextBox(rtb, "Herd Status: Not FQA")
                                    AppendTextBox(rtb, Environment.NewLine)
                                    AppendTextBox(rtb, "Log: Move in date in QA, but move off date not in QA")
                                    AppendTextBox(rtb, Environment.NewLine)
                                End If
                                progressbar.PerformStep()

                            Else
                                'DateMoveInfarm is not QA, but DateMoveOffFarm is QA
                                'If DateMoveOffFarm is QA and Stay days>=70 and last 70days was in QA cert cover then is QA
                                'MsgBox("check MoveOffDate2:" & HerdNo & "-" & DateMoveOffFarm)
                                ' MsgBox("MateMoveInFarm not qa,")
                                If CheckQAStatus(HerdNo, DateMoveOffFarm) Then
                                    '  MsgBox("Check DaysInQA")
                                    ' MsgBox("MateMoveInFarm is in qa,")
                                    '  DaysInQA = GetDaysInQA(DateMoveInFarm, DateMoveOffFarm, HerdNo, eartag)
                                    ' DaysInQA = 0
                                    '  If DaysStayinFarm >= 70 Then
                                    '  MsgBox("5 T,Move in date not in QA, move off date in QA, and DaysStayinFarm >=70")
                                    QALog("T", "Move in date not in QA, move off date in QA, and DaysStayinFarm >=70", DaysInQA, DaysStayinFarm, eartag, HerdNo)
                                    AppendTextBox(rtb, "Herd Status: FQA")
                                    AppendTextBox(rtb, Environment.NewLine)
                                    AppendTextBox(rtb, "Days In Farm: " & DaysStayinFarm)
                                    AppendTextBox(rtb, Environment.NewLine)
                                    AppendTextBox(rtb, "Log: Move in date not in QA, move off date in QA, and DaysStayinFarm >=70")
                                    AppendTextBox(rtb, Environment.NewLine)
                                    ''If CheckQAStatus(HerdNo, DateAdd(DateInterval.Day, -70, DateMoveOffFarm)) Then

                                    ''    QALog("T", "Move in date not in QA, move off date in QA, and last 70 days in QA", DaysInQA, eartag, HerdNo)
                                    ''    AppendTextBox(rtb, "Herd Status: FQA")
                                    ''    AppendTextBox(rtb, Environment.NewLine)
                                    ''    AppendTextBox(rtb, "Days In QA: " & DaysInQA)
                                    ''    AppendTextBox(rtb, Environment.NewLine)
                                    ''    AppendTextBox(rtb, "Log: Move in date not in QA, move off date in QA, and last 70 days in QA")
                                    ''    AppendTextBox(rtb, Environment.NewLine)
                                    ''Else
                                    ''    QAvalue = QAvalue + 1
                                    ''    QALog("F", "Move in date not in QA, move off date in QA, but last 70 days are not in QA", DaysInQA, eartag, HerdNo)
                                    ''    AppendTextBox(rtb, "Herd Status: Not FQA")
                                    ''    AppendTextBox(rtb, Environment.NewLine)
                                    ''    AppendTextBox(rtb, "Days In QA: " & DaysInQA)
                                    ''    AppendTextBox(rtb, Environment.NewLine)
                                    ''    AppendTextBox(rtb, "Log: Move in date not in QA, move off date in QA, but last 70 days are not in QA")
                                    ''    AppendTextBox(rtb, Environment.NewLine)
                                    ''End If
                                    ''Else
                                    ''    QAvalue = QAvalue + 1
                                    ''    QALog("F", "Move in date not in QA, move off date in QA, but serve Less than 70 Days", DaysInQA, eartag, HerdNo)
                                    ''    AppendTextBox(rtb, "Herd Status: Not FQA")
                                    ''    AppendTextBox(rtb, Environment.NewLine)
                                    ''    AppendTextBox(rtb, "Days In QA: " & DaysInQA)
                                    ''    AppendTextBox(rtb, Environment.NewLine)
                                    ''    AppendTextBox(rtb, "Log: Move in date not in QA, move off date in QA, but serve Less than 70 Days")
                                    ''    AppendTextBox(rtb, Environment.NewLine)
                                    ''End If
                                Else
                                    QAvalue = QAvalue + 1
                                    '      MsgBox("6 F, not in QA")
                                    QALog("F", "not in QA", DaysInQA, DaysStayinFarm, eartag, HerdNo)
                                    AppendTextBox(rtb, "Herd Status: Not FQA")
                                    AppendTextBox(rtb, Environment.NewLine)
                                End If
                                progressbar.PerformStep()
                            End If

                            AppendTextBox(rtb, "------------------")
                            AppendTextBox(rtb, Environment.NewLine)
                            AppendTextBox(rtb, Environment.NewLine)
                        End While
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MsgBox("Error in NewBQASStatus:" & ex.Message)
        End Try
    End Sub
    'Created Date: 10/12/2015  By: cyang
    Function CheckQAStatus(ByVal Herdno As String, ByVal MoveOffdate As Date) As Boolean
        Dim tf As Boolean = False
        Try

            Dim BQASLookup As New ie.bordbia.qas.CheckHerdStatus
            tf = BQASLookup.QueryBeefDate("Linden Foods (Primal Facility)", "jkjt73", Herdno, MoveOffdate)
            BQASLookup.Dispose()
            '    Threading.Thread.Sleep(2000)
        Catch ex As Exception
            MsgBox("Error in Checking QA Status:" & ex.Message)
        End Try
        Return tf
    End Function
    'Created Date: 10/12/2015  By: cyang
    Private Sub QALog(ByVal QAStatus As String, ByVal StatusLog As String, ByVal QADays As Integer, ByVal DaysStayinFarm As Integer, ByVal EarTag As String, ByVal HerdNo As String)
        Try
            runNonQuery("Update ROIMove set HerdStatus='" & QAStatus & "', StatusLog='" & StatusLog & "',DaysStayInFarm=" & DaysStayinFarm & ", InQADays=" & QADays.ToString & " where Eartag='" & EarTag & "' and Herdno='" & HerdNo & "'")
        Catch ex As Exception
            MsgBox("Error On QALog:" & ex.Message)
        End Try
    End Sub
    Private Sub UpdateMoveEartag(ByVal QAStatus As String, ByVal DaysStayinFarm As Integer, ByVal EarTag As String, ByVal LastHerdNo As String)
        Try
            runNonQuery("Update ROIMoveEartag set DaysOnLastFarm='" & DaysStayinFarm & "', lastfarmherdno='" & LastHerdNo & "',BQAS='" & QAStatus & "', LastUpdated=GetDate()  where Eartag='" & EarTag & "'")
        Catch ex As Exception
            MsgBox("Error On QALog:" & ex.Message)
        End Try
    End Sub
    Private Sub UpdateKillLine(ByVal DaysStayinFarm As Integer, ByVal EarTag As String)
        Try
            runNonQuery("Update [Multiflex_Linden].[dbo].[Beef_AphisData_ANL] set [DaysOnLastFarm]='" & DaysStayinFarm & "' where AnimalTagNumber='" & EarTag & "'")
        Catch ex As Exception
            MsgBox("Error On QALog:" & ex.Message)
        End Try
    End Sub


    Private Function CheckLast70Days(ByVal MoveOffDate As Date, ByVal HerdNo As String) As String
        Dim str As String = ""
        If CheckQAStatus(HerdNo, DateAdd(DateInterval.Day, -70, MoveOffDate)) Then
            str = ">=70 Days"
        Else
            str = "<70 Days"
        End If
        Return str
    End Function
    'Created Date: 10/12/2015  By: cyang
    Private Function GetDaysInQA(ByVal MoveInDate As Date, ByVal MoveOffDate As Date, ByVal HerdNo As String, ByVal eartag As String) As String
        ' MoveInDate is not QA, and MoveOffDate is QA
        Dim DateFrom As Date = MoveInDate
        Dim DateTo As Date = MoveOffDate
        Dim DateValid As Date
        Dim Days As Integer = 0
        Dim BQASLookup As New ie.bordbia.qas.CheckHerdStatus
        Dim MiddleDays As Integer = 0
        Dim FindTheValidDate As Boolean = False
        Try
            ' below is the "mid-Searching algorithm" to find out the validate of a QA certificate 
            While FindTheValidDate = False
                ' MsgBox(DateDiff(DateInterval.Day, DateFrom, DateTo))
                MiddleDays = CInt(Round((DateDiff(DateInterval.Day, DateFrom, DateTo) / 2), 0))
                'MsgBox("DaysLenght:" & DateDiff(DateInterval.Day, MoveInDate, MoveOffDate) & " MiddleDays:" & MiddleDays & "  DateFrom:" & DateFrom & " - DateTo:" & DateTo & " Date Check: " & DateAdd(DateInterval.Day, MiddleDays, DateFrom).ToString)
                'if DateTo is in QA and DateTo-1 is not in QA, then the DateTo is the AQ valid date

                'If BQASLookup.QueryBeefDate("Linden Foods (Primal Facility)", "jkjt73", HerdNo, DateAdd(DateInterval.Day, MiddleDays, DateFrom)) Then
                If CheckQAStatus(HerdNo, DateAdd(DateInterval.Day, MiddleDays, DateFrom)) Then
                    DateTo = DateAdd(DateInterval.Day, MiddleDays, DateFrom)
                    'If BQASLookup.QueryBeefDate("Linden Foods (Primal Facility)", "jkjt73", HerdNo, DateAdd(DateInterval.Day, MiddleDays - 1, DateFrom)) = False Then
                    If CheckQAStatus(HerdNo, DateAdd(DateInterval.Day, MiddleDays - 1, DateFrom)) = False Then
                        FindTheValidDate = True
                        DateValid = DateAdd(DateInterval.Day, MiddleDays, DateFrom)
                        runNonQuery("update ROIMove set CertValidDate='" & DateValid.ToString("yyyy-MM-dd") & "' where Eartag='" & eartag & "' and Herdno='" & HerdNo & "'")
                    End If
                Else
                    DateFrom = DateAdd(DateInterval.Day, MiddleDays, DateFrom)
                End If
            End While
        Catch ex As Exception
            MsgBox("Error in GetDaysInQA:  " & ex.Message)
        End Try
        If FindTheValidDate Then
            Days = DateDiff(DateInterval.Day, DateValid, MoveOffDate)
        End If
        Return Days.ToString
    End Function

    Private Delegate Sub AppendTextBoxDelegate(ByVal TB As RichTextBox, ByVal txt As String)
    Private Sub AppendTextBox(ByVal TB As RichTextBox, ByVal txt As String)
        If TB.InvokeRequired Then
            TB.Invoke(New AppendTextBoxDelegate(AddressOf AppendTextBox), New Object() {TB, txt})
        Else
            TB.AppendText(txt)
        End If
    End Sub


End Class
