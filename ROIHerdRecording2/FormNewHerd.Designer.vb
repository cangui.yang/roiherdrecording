﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormNewHerd
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormNewHerd))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.tb_County = New System.Windows.Forms.TextBox()
        Me.tb_Town = New System.Windows.Forms.TextBox()
        Me.tb_Name = New System.Windows.Forms.TextBox()
        Me.tb_HerdNo = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.bt_Update = New System.Windows.Forms.Button()
        Me.bt_Close = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.tb_County)
        Me.GroupBox1.Controls.Add(Me.tb_Town)
        Me.GroupBox1.Controls.Add(Me.tb_Name)
        Me.GroupBox1.Controls.Add(Me.tb_HerdNo)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(339, 138)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Herd Info"
        '
        'tb_County
        '
        Me.tb_County.Location = New System.Drawing.Point(66, 104)
        Me.tb_County.Name = "tb_County"
        Me.tb_County.Size = New System.Drawing.Size(258, 20)
        Me.tb_County.TabIndex = 4
        '
        'tb_Town
        '
        Me.tb_Town.Location = New System.Drawing.Point(66, 78)
        Me.tb_Town.Name = "tb_Town"
        Me.tb_Town.Size = New System.Drawing.Size(258, 20)
        Me.tb_Town.TabIndex = 3
        '
        'tb_Name
        '
        Me.tb_Name.Location = New System.Drawing.Point(66, 52)
        Me.tb_Name.Name = "tb_Name"
        Me.tb_Name.Size = New System.Drawing.Size(258, 20)
        Me.tb_Name.TabIndex = 2
        '
        'tb_HerdNo
        '
        Me.tb_HerdNo.Location = New System.Drawing.Point(66, 26)
        Me.tb_HerdNo.Name = "tb_HerdNo"
        Me.tb_HerdNo.Size = New System.Drawing.Size(258, 20)
        Me.tb_HerdNo.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 107)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "County:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 81)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(37, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Town:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 55)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Name:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Herd No.:"
        '
        'bt_Update
        '
        Me.bt_Update.Image = Global.ROIHerdRecording2.My.Resources.Resources.OK_32
        Me.bt_Update.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bt_Update.Location = New System.Drawing.Point(78, 156)
        Me.bt_Update.Name = "bt_Update"
        Me.bt_Update.Size = New System.Drawing.Size(85, 36)
        Me.bt_Update.TabIndex = 6
        Me.bt_Update.Text = "Update"
        Me.bt_Update.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.bt_Update.UseVisualStyleBackColor = True
        '
        'bt_Close
        '
        Me.bt_Close.Image = Global.ROIHerdRecording2.My.Resources.Resources.delete
        Me.bt_Close.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bt_Close.Location = New System.Drawing.Point(196, 156)
        Me.bt_Close.Name = "bt_Close"
        Me.bt_Close.Size = New System.Drawing.Size(85, 36)
        Me.bt_Close.TabIndex = 7
        Me.bt_Close.Text = "Close"
        Me.bt_Close.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.bt_Close.UseVisualStyleBackColor = True
        '
        'FormNewHerd
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(361, 201)
        Me.Controls.Add(Me.bt_Close)
        Me.Controls.Add(Me.bt_Update)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FormNewHerd"
        Me.Text = "FormNewHerd"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents tb_County As TextBox
    Friend WithEvents tb_Town As TextBox
    Friend WithEvents tb_Name As TextBox
    Friend WithEvents tb_HerdNo As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents bt_Update As Button
    Friend WithEvents bt_Close As Button
End Class
