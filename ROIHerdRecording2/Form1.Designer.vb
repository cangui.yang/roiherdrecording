﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tb_Barcodes1 = New System.Windows.Forms.TextBox()
        Me.tb_Barcodes2 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tb_Eartag = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tb_moves = New System.Windows.Forms.TextBox()
        Me.tb_BirthHerdNo = New System.Windows.Forms.TextBox()
        Me.dtp_DateOfBirth = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.bt_Update = New System.Windows.Forms.Button()
        Me.bt_NewSearch = New System.Windows.Forms.Button()
        Me.bt_SearchAdd = New System.Windows.Forms.Button()
        Me.LabelEarTag = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.dtp_PasswordRec = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cb_Status = New System.Windows.Forms.ComboBox()
        Me.tb_NiMove = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cb_ROIviaNI = New System.Windows.Forms.CheckBox()
        Me.tb_HerdNo = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.dgv_Movements = New System.Windows.Forms.DataGridView()
        Me.HerdnoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MoveDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HerdStatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DaysStayInFarm = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateMoveOffFarm = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FarmOfOrigin = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DeleteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ROIMoveBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AMPSLindenBeefDataSet = New ROIHerdRecording2.AMPSLindenBeefDataSet()
        Me.bt_AddMove = New System.Windows.Forms.Button()
        Me.bt_EditHerd = New System.Windows.Forms.Button()
        Me.dtp_MoveDate = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.bt_CheckSQStatus = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.lb_QAStatus = New System.Windows.Forms.Label()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.dtp_DownloadDatabyDate = New System.Windows.Forms.DateTimePicker()
        Me.bt_Download = New System.Windows.Forms.Button()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.tb_AnimalFQAResultKeywords = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.KillNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EarTagDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QAStatusDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LogDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LastUpdateDateTimeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AnimalQAStatusBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Button_UpdateUKCattleMoves = New System.Windows.Forms.Button()
        Me.TextBox_UKCattleMoveNo = New System.Windows.Forms.TextBox()
        Me.TextBox_UKCattleEartag = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.ROIMoveTableAdapter = New ROIHerdRecording2.AMPSLindenBeefDataSetTableAdapters.ROIMoveTableAdapter()
        Me.AnimalQAStatusTableAdapter = New ROIHerdRecording2.AMPSLindenBeefDataSetTableAdapters.AnimalQAStatusTableAdapter()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgv_Movements, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        CType(Me.ROIMoveBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AMPSLindenBeefDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AnimalQAStatusBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(11, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Barcodes:"
        '
        'tb_Barcodes1
        '
        Me.tb_Barcodes1.Location = New System.Drawing.Point(87, 19)
        Me.tb_Barcodes1.Name = "tb_Barcodes1"
        Me.tb_Barcodes1.Size = New System.Drawing.Size(238, 20)
        Me.tb_Barcodes1.TabIndex = 1
        '
        'tb_Barcodes2
        '
        Me.tb_Barcodes2.Location = New System.Drawing.Point(331, 19)
        Me.tb_Barcodes2.Name = "tb_Barcodes2"
        Me.tb_Barcodes2.Size = New System.Drawing.Size(202, 20)
        Me.tb_Barcodes2.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(11, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "EarTag:"
        '
        'tb_Eartag
        '
        Me.tb_Eartag.Location = New System.Drawing.Point(87, 45)
        Me.tb_Eartag.Name = "tb_Eartag"
        Me.tb_Eartag.ReadOnly = True
        Me.tb_Eartag.Size = New System.Drawing.Size(238, 20)
        Me.tb_Eartag.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(11, 74)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(42, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Moves:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(11, 100)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(69, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Date of Birth:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(11, 126)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(74, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Birth HerdNo.:"
        '
        'tb_moves
        '
        Me.tb_moves.Location = New System.Drawing.Point(87, 71)
        Me.tb_moves.Name = "tb_moves"
        Me.tb_moves.Size = New System.Drawing.Size(238, 20)
        Me.tb_moves.TabIndex = 8
        '
        'tb_BirthHerdNo
        '
        Me.tb_BirthHerdNo.Location = New System.Drawing.Point(87, 123)
        Me.tb_BirthHerdNo.Name = "tb_BirthHerdNo"
        Me.tb_BirthHerdNo.Size = New System.Drawing.Size(238, 20)
        Me.tb_BirthHerdNo.TabIndex = 11
        '
        'dtp_DateOfBirth
        '
        Me.dtp_DateOfBirth.CustomFormat = "dd/MM/yyyy"
        Me.dtp_DateOfBirth.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_DateOfBirth.Location = New System.Drawing.Point(87, 98)
        Me.dtp_DateOfBirth.Name = "dtp_DateOfBirth"
        Me.dtp_DateOfBirth.Size = New System.Drawing.Size(238, 20)
        Me.dtp_DateOfBirth.TabIndex = 10
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.bt_Update)
        Me.GroupBox1.Controls.Add(Me.bt_NewSearch)
        Me.GroupBox1.Controls.Add(Me.bt_SearchAdd)
        Me.GroupBox1.Controls.Add(Me.tb_Barcodes1)
        Me.GroupBox1.Controls.Add(Me.dtp_DateOfBirth)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.tb_BirthHerdNo)
        Me.GroupBox1.Controls.Add(Me.tb_Barcodes2)
        Me.GroupBox1.Controls.Add(Me.tb_moves)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.tb_Eartag)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(539, 158)
        Me.GroupBox1.TabIndex = 12
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Animal Info"
        '
        'bt_Update
        '
        Me.bt_Update.Image = Global.ROIHerdRecording2.My.Resources.Resources.Apply
        Me.bt_Update.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.bt_Update.Location = New System.Drawing.Point(449, 45)
        Me.bt_Update.Name = "bt_Update"
        Me.bt_Update.Size = New System.Drawing.Size(84, 98)
        Me.bt_Update.TabIndex = 14
        Me.bt_Update.Text = "Update"
        Me.bt_Update.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.bt_Update.UseVisualStyleBackColor = True
        '
        'bt_NewSearch
        '
        Me.bt_NewSearch.Image = Global.ROIHerdRecording2.My.Resources.Resources.alert
        Me.bt_NewSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bt_NewSearch.Location = New System.Drawing.Point(331, 97)
        Me.bt_NewSearch.Name = "bt_NewSearch"
        Me.bt_NewSearch.Size = New System.Drawing.Size(112, 47)
        Me.bt_NewSearch.TabIndex = 13
        Me.bt_NewSearch.Text = "New Search"
        Me.bt_NewSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.bt_NewSearch.UseVisualStyleBackColor = True
        '
        'bt_SearchAdd
        '
        Me.bt_SearchAdd.Image = Global.ROIHerdRecording2.My.Resources.Resources.Search_32
        Me.bt_SearchAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.bt_SearchAdd.Location = New System.Drawing.Point(331, 45)
        Me.bt_SearchAdd.Name = "bt_SearchAdd"
        Me.bt_SearchAdd.Size = New System.Drawing.Size(112, 45)
        Me.bt_SearchAdd.TabIndex = 12
        Me.bt_SearchAdd.Text = "Search/Add"
        Me.bt_SearchAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.bt_SearchAdd.UseVisualStyleBackColor = True
        '
        'LabelEarTag
        '
        Me.LabelEarTag.AutoSize = True
        Me.LabelEarTag.Location = New System.Drawing.Point(301, 49)
        Me.LabelEarTag.Name = "LabelEarTag"
        Me.LabelEarTag.Size = New System.Drawing.Size(0, 13)
        Me.LabelEarTag.TabIndex = 15
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.LabelEarTag)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.dtp_PasswordRec)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.cb_Status)
        Me.GroupBox2.Controls.Add(Me.tb_NiMove)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.cb_ROIviaNI)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(7, 172)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(538, 74)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Additional Info"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(12, 44)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(120, 13)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Passport Receive Date:"
        '
        'dtp_PasswordRec
        '
        Me.dtp_PasswordRec.CustomFormat = "dd/MM/yyyy"
        Me.dtp_PasswordRec.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_PasswordRec.Location = New System.Drawing.Point(143, 43)
        Me.dtp_PasswordRec.Name = "dtp_PasswordRec"
        Me.dtp_PasswordRec.Size = New System.Drawing.Size(114, 20)
        Me.dtp_PasswordRec.TabIndex = 16
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(284, 19)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(40, 13)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Status:"
        '
        'cb_Status
        '
        Me.cb_Status.FormattingEnabled = True
        Me.cb_Status.Items.AddRange(New Object() {"Requested", "Received", "Can Not Provide"})
        Me.cb_Status.Location = New System.Drawing.Point(330, 13)
        Me.cb_Status.Name = "cb_Status"
        Me.cb_Status.Size = New System.Drawing.Size(202, 21)
        Me.cb_Status.TabIndex = 12
        '
        'tb_NiMove
        '
        Me.tb_NiMove.Location = New System.Drawing.Point(143, 15)
        Me.tb_NiMove.Name = "tb_NiMove"
        Me.tb_NiMove.Size = New System.Drawing.Size(114, 20)
        Me.tb_NiMove.TabIndex = 11
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(115, 19)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(17, 13)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Ni"
        '
        'cb_ROIviaNI
        '
        Me.cb_ROIviaNI.AutoSize = True
        Me.cb_ROIviaNI.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cb_ROIviaNI.Checked = True
        Me.cb_ROIviaNI.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cb_ROIviaNI.Location = New System.Drawing.Point(11, 19)
        Me.cb_ROIviaNI.Name = "cb_ROIviaNI"
        Me.cb_ROIviaNI.Size = New System.Drawing.Size(76, 17)
        Me.cb_ROIviaNI.TabIndex = 0
        Me.cb_ROIviaNI.Text = "ROI via NI"
        Me.cb_ROIviaNI.UseVisualStyleBackColor = True
        '
        'tb_HerdNo
        '
        Me.tb_HerdNo.Location = New System.Drawing.Point(86, 16)
        Me.tb_HerdNo.Name = "tb_HerdNo"
        Me.tb_HerdNo.Size = New System.Drawing.Size(120, 20)
        Me.tb_HerdNo.TabIndex = 19
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(12, 22)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(50, 13)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "HerdNo.:"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.dgv_Movements)
        Me.GroupBox3.Controls.Add(Me.bt_AddMove)
        Me.GroupBox3.Controls.Add(Me.bt_EditHerd)
        Me.GroupBox3.Controls.Add(Me.dtp_MoveDate)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.bt_CheckSQStatus)
        Me.GroupBox3.Controls.Add(Me.tb_HerdNo)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(7, 252)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(538, 279)
        Me.GroupBox3.TabIndex = 20
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Movements Info"
        '
        'dgv_Movements
        '
        Me.dgv_Movements.AllowUserToAddRows = False
        Me.dgv_Movements.AllowUserToDeleteRows = False
        Me.dgv_Movements.AllowUserToOrderColumns = True
        Me.dgv_Movements.AutoGenerateColumns = False
        Me.dgv_Movements.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_Movements.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.HerdnoDataGridViewTextBoxColumn, Me.MoveDateDataGridViewTextBoxColumn, Me.HerdStatus, Me.DaysStayInFarm, Me.DateMoveOffFarm, Me.FarmOfOrigin})
        Me.dgv_Movements.ContextMenuStrip = Me.ContextMenuStrip1
        Me.dgv_Movements.DataSource = Me.ROIMoveBindingSource
        Me.dgv_Movements.Location = New System.Drawing.Point(8, 75)
        Me.dgv_Movements.Name = "dgv_Movements"
        Me.dgv_Movements.ReadOnly = True
        Me.dgv_Movements.RowHeadersWidth = 15
        Me.dgv_Movements.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv_Movements.Size = New System.Drawing.Size(523, 196)
        Me.dgv_Movements.TabIndex = 24
        '
        'HerdnoDataGridViewTextBoxColumn
        '
        Me.HerdnoDataGridViewTextBoxColumn.DataPropertyName = "Herdno"
        Me.HerdnoDataGridViewTextBoxColumn.HeaderText = "Herdno"
        Me.HerdnoDataGridViewTextBoxColumn.Name = "HerdnoDataGridViewTextBoxColumn"
        Me.HerdnoDataGridViewTextBoxColumn.ReadOnly = True
        Me.HerdnoDataGridViewTextBoxColumn.Width = 80
        '
        'MoveDateDataGridViewTextBoxColumn
        '
        Me.MoveDateDataGridViewTextBoxColumn.DataPropertyName = "MoveDate"
        DataGridViewCellStyle1.Format = "dd/MMM/yyyy"
        Me.MoveDateDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle1
        Me.MoveDateDataGridViewTextBoxColumn.HeaderText = "DateMoveInFarm"
        Me.MoveDateDataGridViewTextBoxColumn.Name = "MoveDateDataGridViewTextBoxColumn"
        Me.MoveDateDataGridViewTextBoxColumn.ReadOnly = True
        '
        'HerdStatus
        '
        Me.HerdStatus.DataPropertyName = "HerdStatus"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.HerdStatus.DefaultCellStyle = DataGridViewCellStyle2
        Me.HerdStatus.HeaderText = "HerdStatus"
        Me.HerdStatus.Name = "HerdStatus"
        Me.HerdStatus.ReadOnly = True
        Me.HerdStatus.Width = 70
        '
        'DaysStayInFarm
        '
        Me.DaysStayInFarm.DataPropertyName = "DaysStayInFarm"
        Me.DaysStayInFarm.HeaderText = "DaysInFarm"
        Me.DaysStayInFarm.Name = "DaysStayInFarm"
        Me.DaysStayInFarm.ReadOnly = True
        Me.DaysStayInFarm.Width = 80
        '
        'DateMoveOffFarm
        '
        Me.DateMoveOffFarm.DataPropertyName = "DateMoveOffFarm"
        DataGridViewCellStyle3.Format = "dd/MMM/yyyy"
        Me.DateMoveOffFarm.DefaultCellStyle = DataGridViewCellStyle3
        Me.DateMoveOffFarm.HeaderText = "DateMoveOffFarm"
        Me.DateMoveOffFarm.Name = "DateMoveOffFarm"
        Me.DateMoveOffFarm.ReadOnly = True
        '
        'FarmOfOrigin
        '
        Me.FarmOfOrigin.DataPropertyName = "FarmOfOrigin"
        Me.FarmOfOrigin.HeaderText = "FarmOfOrigin"
        Me.FarmOfOrigin.Name = "FarmOfOrigin"
        Me.FarmOfOrigin.ReadOnly = True
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeleteToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(163, 26)
        '
        'DeleteToolStripMenuItem
        '
        Me.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem"
        Me.DeleteToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.DeleteToolStripMenuItem.Text = "Delete All Moves"
        '
        'ROIMoveBindingSource
        '
        Me.ROIMoveBindingSource.DataMember = "ROIMove"
        Me.ROIMoveBindingSource.DataSource = Me.AMPSLindenBeefDataSet
        '
        'AMPSLindenBeefDataSet
        '
        Me.AMPSLindenBeefDataSet.DataSetName = "AMPSLindenBeefDataSet"
        Me.AMPSLindenBeefDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'bt_AddMove
        '
        Me.bt_AddMove.Enabled = False
        Me.bt_AddMove.Image = Global.ROIHerdRecording2.My.Resources.Resources.addNew_32
        Me.bt_AddMove.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.bt_AddMove.Location = New System.Drawing.Point(216, 15)
        Me.bt_AddMove.Name = "bt_AddMove"
        Me.bt_AddMove.Size = New System.Drawing.Size(101, 53)
        Me.bt_AddMove.TabIndex = 23
        Me.bt_AddMove.Text = "Add Move"
        Me.bt_AddMove.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.bt_AddMove.UseVisualStyleBackColor = True
        '
        'bt_EditHerd
        '
        Me.bt_EditHerd.Image = Global.ROIHerdRecording2.My.Resources.Resources.edit
        Me.bt_EditHerd.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.bt_EditHerd.Location = New System.Drawing.Point(323, 15)
        Me.bt_EditHerd.Name = "bt_EditHerd"
        Me.bt_EditHerd.Size = New System.Drawing.Size(101, 53)
        Me.bt_EditHerd.TabIndex = 22
        Me.bt_EditHerd.Text = "Edit Herd"
        Me.bt_EditHerd.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.bt_EditHerd.UseVisualStyleBackColor = True
        '
        'dtp_MoveDate
        '
        Me.dtp_MoveDate.CustomFormat = "dd/MM/yyyy"
        Me.dtp_MoveDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_MoveDate.Location = New System.Drawing.Point(86, 45)
        Me.dtp_MoveDate.Name = "dtp_MoveDate"
        Me.dtp_MoveDate.Size = New System.Drawing.Size(120, 20)
        Me.dtp_MoveDate.TabIndex = 21
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(12, 49)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(63, 13)
        Me.Label10.TabIndex = 20
        Me.Label10.Text = "Move Date:"
        '
        'bt_CheckSQStatus
        '
        Me.bt_CheckSQStatus.Image = Global.ROIHerdRecording2.My.Resources.Resources.Clear_32
        Me.bt_CheckSQStatus.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.bt_CheckSQStatus.Location = New System.Drawing.Point(430, 15)
        Me.bt_CheckSQStatus.Name = "bt_CheckSQStatus"
        Me.bt_CheckSQStatus.Size = New System.Drawing.Size(101, 53)
        Me.bt_CheckSQStatus.TabIndex = 15
        Me.bt_CheckSQStatus.Text = "Check QA"
        Me.bt_CheckSQStatus.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.bt_CheckSQStatus.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.ProgressBar1)
        Me.GroupBox4.Controls.Add(Me.lb_QAStatus)
        Me.GroupBox4.Controls.Add(Me.RichTextBox1)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(557, 9)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(213, 525)
        Me.GroupBox4.TabIndex = 21
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "QA Result"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(7, 489)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(200, 23)
        Me.ProgressBar1.TabIndex = 2
        '
        'lb_QAStatus
        '
        Me.lb_QAStatus.AutoSize = True
        Me.lb_QAStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lb_QAStatus.Location = New System.Drawing.Point(18, 18)
        Me.lb_QAStatus.Name = "lb_QAStatus"
        Me.lb_QAStatus.Size = New System.Drawing.Size(139, 31)
        Me.lb_QAStatus.TabIndex = 1
        Me.lb_QAStatus.Text = "QAStatus"
        '
        'RichTextBox1
        '
        Me.RichTextBox1.BackColor = System.Drawing.SystemColors.Window
        Me.RichTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RichTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox1.ForeColor = System.Drawing.Color.Black
        Me.RichTextBox1.Location = New System.Drawing.Point(6, 58)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical
        Me.RichTextBox1.Size = New System.Drawing.Size(201, 424)
        Me.RichTextBox1.TabIndex = 0
        Me.RichTextBox1.Text = ""
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.ItemSize = New System.Drawing.Size(136, 25)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(784, 570)
        Me.TabControl1.TabIndex = 22
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Controls.Add(Me.GroupBox2)
        Me.TabPage1.Controls.Add(Me.GroupBox4)
        Me.TabPage1.Controls.Add(Me.GroupBox3)
        Me.TabPage1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage1.Location = New System.Drawing.Point(4, 29)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(776, 537)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "     FQA Processor     "
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage2.Controls.Add(Me.LinkLabel1)
        Me.TabPage2.Controls.Add(Me.dtp_DownloadDatabyDate)
        Me.TabPage2.Controls.Add(Me.bt_Download)
        Me.TabPage2.Controls.Add(Me.Label12)
        Me.TabPage2.Controls.Add(Me.tb_AnimalFQAResultKeywords)
        Me.TabPage2.Controls.Add(Me.Label11)
        Me.TabPage2.Controls.Add(Me.DataGridView1)
        Me.TabPage2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage2.Location = New System.Drawing.Point(4, 29)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(776, 537)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "     Animal FQA Result     "
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(583, 10)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(94, 13)
        Me.LinkLabel1.TabIndex = 6
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Bord Bia Web Site"
        '
        'dtp_DownloadDatabyDate
        '
        Me.dtp_DownloadDatabyDate.CustomFormat = "dd/MM/yyyy"
        Me.dtp_DownloadDatabyDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_DownloadDatabyDate.Location = New System.Drawing.Point(400, 7)
        Me.dtp_DownloadDatabyDate.Name = "dtp_DownloadDatabyDate"
        Me.dtp_DownloadDatabyDate.Size = New System.Drawing.Size(95, 20)
        Me.dtp_DownloadDatabyDate.TabIndex = 5
        '
        'bt_Download
        '
        Me.bt_Download.Location = New System.Drawing.Point(501, 5)
        Me.bt_Download.Name = "bt_Download"
        Me.bt_Download.Size = New System.Drawing.Size(75, 23)
        Me.bt_Download.TabIndex = 4
        Me.bt_Download.Text = "Download"
        Me.bt_Download.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(239, 10)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(147, 13)
        Me.Label12.TabIndex = 3
        Me.Label12.Text = "Only display last 1000 records"
        '
        'tb_AnimalFQAResultKeywords
        '
        Me.tb_AnimalFQAResultKeywords.Location = New System.Drawing.Point(96, 7)
        Me.tb_AnimalFQAResultKeywords.Name = "tb_AnimalFQAResultKeywords"
        Me.tb_AnimalFQAResultKeywords.Size = New System.Drawing.Size(137, 20)
        Me.tb_AnimalFQAResultKeywords.TabIndex = 2
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(4, 10)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(89, 13)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "Kill No./ EarTag :"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.KillNoDataGridViewTextBoxColumn, Me.EarTagDataGridViewTextBoxColumn, Me.QAStatusDataGridViewTextBoxColumn, Me.LogDataGridViewTextBoxColumn, Me.LastUpdateDateTimeDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.AnimalQAStatusBindingSource
        Me.DataGridView1.Location = New System.Drawing.Point(5, 34)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersWidth = 20
        Me.DataGridView1.Size = New System.Drawing.Size(765, 497)
        Me.DataGridView1.TabIndex = 0
        '
        'KillNoDataGridViewTextBoxColumn
        '
        Me.KillNoDataGridViewTextBoxColumn.DataPropertyName = "KillNo"
        Me.KillNoDataGridViewTextBoxColumn.HeaderText = "KillNo"
        Me.KillNoDataGridViewTextBoxColumn.Name = "KillNoDataGridViewTextBoxColumn"
        Me.KillNoDataGridViewTextBoxColumn.ReadOnly = True
        Me.KillNoDataGridViewTextBoxColumn.Width = 60
        '
        'EarTagDataGridViewTextBoxColumn
        '
        Me.EarTagDataGridViewTextBoxColumn.DataPropertyName = "EarTag"
        Me.EarTagDataGridViewTextBoxColumn.HeaderText = "EarTag"
        Me.EarTagDataGridViewTextBoxColumn.Name = "EarTagDataGridViewTextBoxColumn"
        Me.EarTagDataGridViewTextBoxColumn.ReadOnly = True
        Me.EarTagDataGridViewTextBoxColumn.Width = 120
        '
        'QAStatusDataGridViewTextBoxColumn
        '
        Me.QAStatusDataGridViewTextBoxColumn.DataPropertyName = "QAStatus"
        Me.QAStatusDataGridViewTextBoxColumn.HeaderText = "QAStatus"
        Me.QAStatusDataGridViewTextBoxColumn.Name = "QAStatusDataGridViewTextBoxColumn"
        Me.QAStatusDataGridViewTextBoxColumn.ReadOnly = True
        Me.QAStatusDataGridViewTextBoxColumn.Width = 70
        '
        'LogDataGridViewTextBoxColumn
        '
        Me.LogDataGridViewTextBoxColumn.DataPropertyName = "Log"
        Me.LogDataGridViewTextBoxColumn.HeaderText = "Log"
        Me.LogDataGridViewTextBoxColumn.Name = "LogDataGridViewTextBoxColumn"
        Me.LogDataGridViewTextBoxColumn.ReadOnly = True
        Me.LogDataGridViewTextBoxColumn.Width = 300
        '
        'LastUpdateDateTimeDataGridViewTextBoxColumn
        '
        Me.LastUpdateDateTimeDataGridViewTextBoxColumn.DataPropertyName = "LastUpdateDateTime"
        DataGridViewCellStyle4.Format = "G"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.LastUpdateDateTimeDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle4
        Me.LastUpdateDateTimeDataGridViewTextBoxColumn.HeaderText = "LastUpdateDateTime"
        Me.LastUpdateDateTimeDataGridViewTextBoxColumn.Name = "LastUpdateDateTimeDataGridViewTextBoxColumn"
        Me.LastUpdateDateTimeDataGridViewTextBoxColumn.ReadOnly = True
        Me.LastUpdateDateTimeDataGridViewTextBoxColumn.Width = 150
        '
        'AnimalQAStatusBindingSource
        '
        Me.AnimalQAStatusBindingSource.DataMember = "AnimalQAStatus"
        Me.AnimalQAStatusBindingSource.DataSource = Me.AMPSLindenBeefDataSet
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Button_UpdateUKCattleMoves)
        Me.TabPage3.Controls.Add(Me.TextBox_UKCattleMoveNo)
        Me.TabPage3.Controls.Add(Me.TextBox_UKCattleEartag)
        Me.TabPage3.Controls.Add(Me.Label14)
        Me.TabPage3.Controls.Add(Me.Label13)
        Me.TabPage3.Location = New System.Drawing.Point(4, 29)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(776, 537)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "GB Cattles MovesNo"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'Button_UpdateUKCattleMoves
        '
        Me.Button_UpdateUKCattleMoves.Enabled = False
        Me.Button_UpdateUKCattleMoves.Location = New System.Drawing.Point(450, 30)
        Me.Button_UpdateUKCattleMoves.Name = "Button_UpdateUKCattleMoves"
        Me.Button_UpdateUKCattleMoves.Size = New System.Drawing.Size(75, 51)
        Me.Button_UpdateUKCattleMoves.TabIndex = 4
        Me.Button_UpdateUKCattleMoves.Text = "Update"
        Me.Button_UpdateUKCattleMoves.UseVisualStyleBackColor = True
        '
        'TextBox_UKCattleMoveNo
        '
        Me.TextBox_UKCattleMoveNo.Location = New System.Drawing.Point(178, 61)
        Me.TextBox_UKCattleMoveNo.Name = "TextBox_UKCattleMoveNo"
        Me.TextBox_UKCattleMoveNo.Size = New System.Drawing.Size(256, 20)
        Me.TextBox_UKCattleMoveNo.TabIndex = 3
        '
        'TextBox_UKCattleEartag
        '
        Me.TextBox_UKCattleEartag.Location = New System.Drawing.Point(178, 30)
        Me.TextBox_UKCattleEartag.Name = "TextBox_UKCattleEartag"
        Me.TextBox_UKCattleEartag.Size = New System.Drawing.Size(256, 20)
        Me.TextBox_UKCattleEartag.TabIndex = 2
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(20, 33)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(106, 13)
        Me.Label14.TabIndex = 1
        Me.Label14.Text = "GB Cattle Eartag:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(20, 64)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(143, 13)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "Number Of Farms Move:"
        '
        'ROIMoveTableAdapter
        '
        Me.ROIMoveTableAdapter.ClearBeforeFill = True
        '
        'AnimalQAStatusTableAdapter
        '
        Me.AnimalQAStatusTableAdapter.ClearBeforeFill = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(808, 591)
        Me.Controls.Add(Me.TabControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.Text = "ROI Movements (4.1.6)"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.dgv_Movements, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        CType(Me.ROIMoveBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AMPSLindenBeefDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AnimalQAStatusBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents tb_Barcodes1 As TextBox
    Friend WithEvents tb_Barcodes2 As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents tb_Eartag As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents tb_moves As TextBox
    Friend WithEvents tb_BirthHerdNo As TextBox
    Friend WithEvents dtp_DateOfBirth As DateTimePicker
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents bt_NewSearch As Button
    Friend WithEvents bt_SearchAdd As Button
    Friend WithEvents bt_Update As Button
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label8 As Label
    Friend WithEvents dtp_PasswordRec As DateTimePicker
    Friend WithEvents Label7 As Label
    Friend WithEvents cb_Status As ComboBox
    Friend WithEvents tb_NiMove As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents cb_ROIviaNI As CheckBox
    Friend WithEvents bt_CheckSQStatus As Button
    Friend WithEvents tb_HerdNo As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents dtp_MoveDate As DateTimePicker
    Friend WithEvents Label10 As Label
    Friend WithEvents bt_EditHerd As Button
    Friend WithEvents bt_AddMove As Button
    Friend WithEvents dgv_Movements As DataGridView
    Friend WithEvents AMPSLindenBeefDataSet As AMPSLindenBeefDataSet
    Friend WithEvents ROIMoveBindingSource As BindingSource
    Friend WithEvents ROIMoveTableAdapter As AMPSLindenBeefDataSetTableAdapters.ROIMoveTableAdapter
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents DeleteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents RichTextBox1 As RichTextBox
    Friend WithEvents lb_QAStatus As Label
    Friend WithEvents ProgressBar1 As ProgressBar
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents AnimalQAStatusBindingSource As BindingSource
    Friend WithEvents AnimalQAStatusTableAdapter As AMPSLindenBeefDataSetTableAdapters.AnimalQAStatusTableAdapter
    Friend WithEvents tb_AnimalFQAResultKeywords As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents bt_Download As Button
    Friend WithEvents dtp_DownloadDatabyDate As DateTimePicker
    Friend WithEvents LinkLabel1 As LinkLabel
    Friend WithEvents KillNoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents EarTagDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents QAStatusDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents LogDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents LastUpdateDateTimeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents HerdnoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MoveDateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents HerdStatus As DataGridViewTextBoxColumn
    Friend WithEvents DaysStayInFarm As DataGridViewTextBoxColumn
    Friend WithEvents DateMoveOffFarm As DataGridViewTextBoxColumn
    Friend WithEvents FarmOfOrigin As DataGridViewCheckBoxColumn
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents Button_UpdateUKCattleMoves As Button
    Friend WithEvents TextBox_UKCattleMoveNo As TextBox
    Friend WithEvents TextBox_UKCattleEartag As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents LabelEarTag As Label
End Class
